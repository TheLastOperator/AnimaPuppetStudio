#include "application.h"

#include <QStandardPaths>
#include <QFile>
#include <QTextStream>
#include <QApplication>

Application::Application(int &argc, char **argv) : SingleApplication(argc, argv)
{
    m_settings = new QSettings("apsconfig.ini", QSettings::IniFormat);

    m_fileManager = new FileManager(this);
    m_serialBridge = new SerialBridge(this);
    m_trackFactory = new TrackFactory(this);

    m_settings->setValue("workspacePath", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/AnimaPuppetStudioWorkspace");
    //...

    m_workspace = new Workspace();
    m_window = new MainWindow();

    QFile f(":qdarkstyle/style.qss");
    if (!f.exists()) {
        printf("Unable to set stylesheet, file not found\n");
    }
    else {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        this->setStyleSheet(ts.readAll());
    }

}

Application::~Application()
{

}

Workspace *Application::workspace() const
{
    return m_workspace;
}

QSettings *Application::settings() const
{
    return m_settings;
}

Application *Application::instance()
{
    return static_cast<Application *>(QApplication::instance());
}

FileManager *Application::fileManager() const
{
    return m_fileManager;
}

MainWindow *Application::window() const
{
    return m_window;
}

SerialBridge *Application::serialBridge() const
{
    return m_serialBridge;
}

TrackFactory *Application::trackFactory() const
{
    return m_trackFactory;
}
