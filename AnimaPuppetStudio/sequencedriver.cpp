#include "sequencedriver.h"

#include "application.h"

#include "models/trackdata.h"

QList<SequenceDriver*> SequenceDriver::m_drivers = QList<SequenceDriver*>();

SequenceDriver::SequenceDriver(Sequence *sequence, QObject *parent) : QObject(parent)
{
    m_drivers.append(this);

    m_lastTickPosition = 0;

    m_player = new QMediaPlayer;
    m_player->setNotifyInterval(50);
    m_sequence = sequence;

    loadMedia(m_sequence->audioPath());

    // HUGE FORWARDNESS
    connect(m_player, SIGNAL(audioAvailableChanged(bool)), this, SIGNAL(audioAvailableChanged(bool)));
    connect(m_player, SIGNAL(error(QMediaPlayer::Error)), this, SIGNAL(error(QMediaPlayer::Error)));
    connect(m_player, SIGNAL(mutedChanged(bool)), this, SIGNAL(mutedChanged(bool)));
    connect(m_player, SIGNAL(stateChanged(QMediaPlayer::State)), this, SIGNAL(stateChanged(QMediaPlayer::State)));
    connect(m_player, SIGNAL(volumeChanged(int)), this, SIGNAL(volumeChanged(int)));

    connect(m_player, SIGNAL(durationChanged(qint64)), this, SLOT(updateDuration(qint64)));
    connect(m_player, SIGNAL(positionChanged(qint64)), this, SLOT(updateTimer(qint64)));
    connect(m_sequence, SIGNAL(audioPathChanged(QString)), this, SLOT(loadMedia(QString)));

    connect(Application::instance()->serialBridge(), SIGNAL(remotePlayRequest()), this, SLOT(play()));
}

SequenceDriver::~SequenceDriver()
{
    m_drivers.removeAll(this);
    delete m_player;
}

qint64 SequenceDriver::position()
{
    return m_player->position();
}

qint64 SequenceDriver::duration()
{
    return m_player->duration();
}

void SequenceDriver::play()
{
    pauseAll();
    m_player->play();
}

void SequenceDriver::pause()
{
    m_player->pause();
}

void SequenceDriver::stop()
{
    m_player->stop();
}

void SequenceDriver::togglePlay()
{
    if(m_player->state() == QMediaPlayer::PlayingState) {
        pause();
    }
    else {
        play();
    }
}

void SequenceDriver::pauseAll()
{
    for(SequenceDriver *driver : m_drivers) {
        driver->pause();
    }
}

void SequenceDriver::setMuted(bool muted)
{
    m_player->setMuted(muted);
}

void SequenceDriver::setPosition(qint64 position)
{
    m_player->setPosition(position);
}

void SequenceDriver::setVolume(int volume)
{
    m_player->setVolume(volume);
}

void SequenceDriver::loadMedia(QString path)
{
    QDir dir(m_sequence->project()->path());
    dir.cd("..");
    m_player->setMedia(QUrl::fromLocalFile(dir.absoluteFilePath(path)));
}

void SequenceDriver::updateTimer(qint64 position)
{
    if(m_lastTickPosition != position) {
        emit positionChanged(position);
        m_lastTickPosition = position;
        onTimerTick(position);
    }
    if(position == m_player->duration()) {
        emit finished();
    }
}

void SequenceDriver::updateDuration(qint64 duration)
{
    emit durationChanged(duration);
}

void SequenceDriver::onTimerTick(qint64 tick)
{
    Application::instance()->window()->printToLogs("Transmiting sequence frame n° " + QString::number(tick));
    // This dirty part is a placeholder to the full modular system.. TODO
    for(int i = 0; i < m_sequence->tracks().size(); i++) {
        Track *track = m_sequence->tracks().at(i);
        if(track->trackData() != nullptr) {
            qint8 value = track->trackData()->valueAt(tick);
            emit trackUpdated(i, value);
            Application::instance()->window()->printToLogs("    Track : " + track->name());
            Application::instance()->window()->printToLogs("      Value: " + QString::number(value));
            if(track->receivers().length() != 0)
                Application::instance()->window()->printToLogs("      Receivers:");
            for(Receiver *receiver : track->receivers()) {
                Application::instance()->window()->printToLogs("        " + QString("%1:%2").arg(receiver->uuid(), 16, 16, QChar('0')).arg(receiver->port()));
                Application::instance()->serialBridge()->sendDataToReceiver(receiver, receiver->inverted() ? 127 - value : value);
            }
        }
    }
    Application::instance()->window()->printToLogs("done.\n");
}
Sequence *SequenceDriver::sequence() const
{
    return m_sequence;
}
