#include "track.h"
#include "sequence.h"

Track::Track(Sequence *parent, TrackData *trackData) : TreeItem(parent)
{
    m_sequence = parent;
    m_trackData = trackData;
}

Track::~Track()
{
    qDeleteAll(m_receivers);
    delete m_trackData;
}

TreeItem *Track::child(int row)
{
    return m_receivers.value(row);
}

int Track::childCount() const
{
    return m_receivers.count();
}

int Track::columnCount() const
{
    return 1;
}

QVariant Track::data(int column) const
{
    Q_UNUSED(column);
    return name();
}

int Track::row() const
{
    return m_sequence->tracks().indexOf(const_cast<Track*>(this));
}

TreeItem *Track::parent()
{
    return m_sequence;
}

int Track::itemType()
{
    return 2;
}

Sequence *Track::sequence() const
{
    return m_sequence;
}

QList<Receiver *> Track::receivers() const
{
    return m_receivers;
}

void Track::addReceiver(Receiver *receiver)
{
    m_receivers.append(receiver);
}

void Track::deleteReceiver(Receiver *receiver)
{
    m_receivers.removeAll(receiver);
    delete receiver;
}

TrackData *Track::trackData() const
{
    return m_trackData;
}

void Track::setTrackData(TrackData *trackData)
{
    m_trackData = trackData;
}

