#ifndef TRACK_H
#define TRACK_H

#include "treeitem.h"
#include "receiver.h"
#include "trackdata.h"

class Sequence;

class Track : public TreeItem
{
public:
    Track(Sequence *parent, TrackData *trackData = 0);
    ~Track();

    TreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    TreeItem *parent();
    int itemType();

    Sequence *sequence() const;

    QList<Receiver *> receivers() const;

    void addReceiver(Receiver *receiver);
    void deleteReceiver(Receiver *receiver);

    //
    //

    TrackData *trackData() const;
    void setTrackData(TrackData *trackData);

private:
    Sequence *m_sequence;
    QList<Receiver *> m_receivers;
    TrackData *m_trackData;

};

#endif // TRACK_H
