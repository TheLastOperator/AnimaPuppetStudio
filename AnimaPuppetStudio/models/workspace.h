#ifndef WORKSPACE_H
#define WORKSPACE_H

#include "project.h"
#include "sequence.h"

#include <QAbstractItemModel>
#include <QList>

class Workspace : public QAbstractItemModel
{
public:
    Workspace();
    ~Workspace();

    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;


    QList<Project *> projects() const;
    void addProject(Project *project);
    void addSequence(Project *project, Sequence *sequence);
    void addSequence(Project *project, QString name);
    void addTrack(Sequence *sequence, QString name, QString type);
    void addReceiver(Track *track, Receiver *receiver);

    void deleteProject(Project *project);
    void deleteSequence(Sequence *sequence);
    void deleteTrack(Track *track);
    void deleteReceiver(Receiver *receiver);

    void renameProject(Project *project, QString name);
    void renameSequence(Sequence *sequence, QString name);
    void renameTrack(Track *track, QString name);

    void moveSequence(Project *parent, int from, int to);


private:
    QList<Project*> m_projects;
};

#endif // WORKSPACE_H
