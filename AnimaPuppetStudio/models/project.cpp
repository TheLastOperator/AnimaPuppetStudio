#include "project.h"
#include "workspace.h"

Project::Project(Workspace *parent) : TreeItem(parent)
{
    m_workspace = parent;
}

Project::~Project()
{
    qDeleteAll(m_sequences);
}

TreeItem *Project::child(int row)
{
    return m_sequences.value(row);
}

int Project::childCount() const
{
    return m_sequences.count();
}

int Project::columnCount() const
{
    return 1;
}

QVariant Project::data(int column) const
{
    Q_UNUSED(column);
    return name();
}

int Project::row() const
{
    return m_workspace->projects().indexOf(const_cast<Project*>(this));
}

TreeItem *Project::parent()
{
    return nullptr;
}

int Project::itemType()
{
    return 0;
}

QList<Sequence *> Project::sequences() const
{
    return m_sequences;
}

void Project::addSequence(Sequence *sequence)
{
    m_sequences.append(sequence);
}

void Project::deleteSequence(Sequence *sequence)
{
    m_sequences.removeAll(sequence);
    delete sequence;
}

void Project::moveSequence(int from, int to)
{
    m_sequences.move(from, to);
}

QString Project::path() const
{
    return m_path;
}

void Project::setPath(const QString &path)
{
    m_path = path;
}


