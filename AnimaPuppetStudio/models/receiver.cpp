#include "receiver.h"
#include "track.h"

Receiver::Receiver(Track *parent) : TreeItem(parent)
{
    m_track = parent;
    m_uuid = 0;
    m_port = 0;
}

Receiver::~Receiver()
{

}

TreeItem *Receiver::child(int row)
{
    Q_UNUSED(row);
    return nullptr;
}

int Receiver::childCount() const
{
    return 0;
}

int Receiver::columnCount() const
{
    return 1;
}

QVariant Receiver::data(int column) const
{
    Q_UNUSED(column);
    return name();
}

int Receiver::row() const
{
    return m_track->receivers().indexOf(const_cast<Receiver*>(this));
}

TreeItem *Receiver::parent()
{
    return m_track;
}

int Receiver::itemType()
{
    return 3;
}

Track *Receiver::track()
{
    return m_track;
}
quint64 Receiver::uuid() const
{
    return m_uuid;
}

void Receiver::setUuid(const quint64 &uuid)
{
    m_uuid = uuid;
}
qint8 Receiver::port() const
{
    return m_port;
}

void Receiver::setPort(const qint8 &port)
{
    if(port < 64 && port >= 0)
        m_port = port;
}
bool Receiver::inverted() const
{
    return m_inverted;
}

void Receiver::setInverted(bool inverted)
{
    m_inverted = inverted;
}

bool Receiver::updateCache(quint8 data)
{
    if(data == m_lastDataSent && !m_lastDataTimer.hasExpired(1000)) {
        return false;
    }
    else {
        m_lastDataSent = data;
        m_lastDataTimer.start();
        return true;
    }
}




