#ifndef TREEITEM_H
#define TREEITEM_H

#include <QObject>
#include <QVariant>

class TreeItem : public QObject
{
    Q_OBJECT
public:
    TreeItem(QObject *parent);
    virtual ~TreeItem();

    virtual TreeItem *child(int row) = 0;
    virtual int childCount() const = 0;
    virtual int columnCount() const = 0;
    virtual QVariant data(int column) const = 0;
    virtual int row() const = 0;
    virtual TreeItem *parent() = 0;
    virtual int itemType() = 0;

    QString name() const;
    void setName(const QString &name);

signals:
    void renamed(QString);

private:
    QString m_name;
};

#endif // TREEITEM_H
