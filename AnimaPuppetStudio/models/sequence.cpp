#include "sequence.h"

#include "project.h"

Sequence::Sequence(Project *parent) : TreeItem(parent)
{
    m_project = parent;

    m_audioPath = "";
}

Sequence::~Sequence()
{
    qDeleteAll(m_tracks);
}

TreeItem *Sequence::child(int row)
{
    return m_tracks.value(row);
}

int Sequence::childCount() const
{
    return m_tracks.count();
}

int Sequence::columnCount() const
{
    return 1;
}

QVariant Sequence::data(int column) const
{
    Q_UNUSED(column);
    return name();
}

int Sequence::row() const
{
    return m_project->sequences().indexOf(const_cast<Sequence*>(this));
}

TreeItem *Sequence::parent()
{
    return m_project;
}

int Sequence::itemType()
{
    return 1;
}

Project *Sequence::project() const
{
    return m_project;
}

QList<Track *> Sequence::tracks() const
{
    return m_tracks;
}

void Sequence::addTrack(Track *track)
{
    m_tracks.append(track);
    emit trackAdded(track);
}

void Sequence::deleteTrack(Track *track)
{
    m_tracks.removeAll(track);
    delete track;
}

QString Sequence::audioPath() const
{
    return m_audioPath;
}

void Sequence::setAudioPath(const QString &audioPath)
{
    m_audioPath = audioPath;
    emit audioPathChanged(m_audioPath);
}

