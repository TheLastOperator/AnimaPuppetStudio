#ifndef RECEIVER_H
#define RECEIVER_H

#include "treeitem.h"

#include <QElapsedTimer>

class Track;

class Receiver : public TreeItem
{
public:
    Receiver(Track *parent);
    ~Receiver();

    TreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    TreeItem *parent();
    int itemType();

    Track *track();

    quint64 uuid() const;
    void setUuid(const quint64 &uuid);

    qint8 port() const;
    void setPort(const qint8 &port);

    bool inverted() const;
    void setInverted(bool inverted);

    bool updateCache(quint8 data);

private:
    Track *m_track;
    quint64 m_uuid;
    qint8 m_port;
    bool m_inverted;

    quint8 m_lastDataSent;
    QElapsedTimer m_lastDataTimer;

};

#endif // RECEIVER_H
