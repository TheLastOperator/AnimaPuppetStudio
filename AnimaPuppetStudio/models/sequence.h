#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "treeitem.h"
#include "track.h"

class Project;

class Sequence : public TreeItem
{
    Q_OBJECT
public:
    Sequence(Project *parent);
    ~Sequence();

    TreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    TreeItem *parent();
    int itemType();

    Project *project() const;

    QList<Track *> tracks() const;

    void addTrack(Track *track);
    void deleteTrack(Track *track);

    QString audioPath() const;
    void setAudioPath(const QString &audioPath);

signals:
    void audioPathChanged(QString);
    void trackAdded(Track*);

private:
    Project *m_project;
    QList<Track *> m_tracks;
    QString m_audioPath;
};

#endif // SEQUENCE_H
