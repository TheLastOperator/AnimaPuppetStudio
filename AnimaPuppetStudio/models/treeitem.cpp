#include "treeitem.h"

TreeItem::TreeItem(QObject *parent) : QObject(parent)
{

}

TreeItem::~TreeItem()
{

}

QString TreeItem::name() const
{
    return m_name;
}

void TreeItem::setName(const QString &name)
{
    m_name = name;
    emit renamed(name);
}
