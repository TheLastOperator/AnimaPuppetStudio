#ifndef TRACKDATA_H
#define TRACKDATA_H

#include <QObject>

class TrackData : public QObject
{
    Q_OBJECT
public:
    virtual ~TrackData() {}
    virtual QString type() = 0;

    /**
     * @brief valueAt
     * @param time in miliseconds
     * @return the value at time
     */
    virtual qint8 valueAt(qint64 time) = 0;
};

#endif // TRACKDATA_H
