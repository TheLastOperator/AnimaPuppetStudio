#ifndef PROJECT_H
#define PROJECT_H

#include "treeitem.h"
#include "sequence.h"

class Workspace;

class Project : public TreeItem
{
public:
    Project(Workspace *parent);
    ~Project();

    TreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    TreeItem *parent();
    int itemType();

    //

    QList<Sequence *> sequences() const;

    void addSequence(Sequence *sequence);
    void deleteSequence(Sequence *sequence);

    void moveSequence(int from, int to);

    QString path() const;
    void setPath(const QString &path);

private:
    Workspace *m_workspace;
    QList<Sequence *> m_sequences;
    QString m_path;

};

#endif // PROJECT_H
