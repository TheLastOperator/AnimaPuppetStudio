#include "workspace.h"

#include <QPixmap>
#include <qdebug.h>
#include <trackplugin.h>

#include "trackdata.h"
#include "application.h"

#include "sequence.h"

Workspace::Workspace()
{

}

Workspace::~Workspace()
{
    qDeleteAll(m_projects);
}

QModelIndex Workspace::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *childItem;

    if (!parent.isValid())
        childItem = m_projects.value(row);
    else
        childItem = (static_cast<TreeItem*>(parent.internalPointer()))->child(row);

    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex Workspace::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (!parentItem)
            return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int Workspace::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        return m_projects.count();
    else
        return static_cast<TreeItem*>(parent.internalPointer())->childCount();
}

int Workspace::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return 1;
}

QList<Project *> Workspace::projects() const
{
    return m_projects;
}

void Workspace::addProject(Project *project)
{
    beginInsertRows(QModelIndex(), m_projects.size(), m_projects.size());
    m_projects.append(project);
    endInsertRows();
}

void Workspace::addSequence(Project *project, Sequence *sequence)
{
    beginInsertRows(createIndex(project->row(), 0, project), project->row(), project->row());
    project->addSequence(sequence);
    endInsertRows();
}

void Workspace::addSequence(Project *project, QString name)
{
    Sequence *sequence = new Sequence(project);
    sequence->setName(name);
    addSequence(project, sequence);
}

void Workspace::addTrack(Sequence *sequence, QString name, QString type)
{
    beginInsertRows(createIndex(sequence->row(), 0, sequence), sequence->row(), sequence->row());
    TrackPlugin *trackPlugin = Application::instance()->trackFactory()->pluginInstance(type);
    TrackData *trackData = nullptr;
    if(trackPlugin != nullptr) {
        trackData = trackPlugin->newTrackData();
    }
    Track *track = new Track(sequence, trackData);
    track->setName(name);
    sequence->addTrack(track);
    endInsertRows();
}

void Workspace::addReceiver(Track *track, Receiver *receiver)
{
    beginInsertRows(createIndex(track->row(), 0, track), track->row(), track->row());
    track->addReceiver(receiver);
    endInsertRows();
}

void Workspace::deleteProject(Project *project)
{
    beginRemoveRows(QModelIndex(), project->row(), project->row());
    m_projects.removeAll(project);
    delete project;
    endRemoveRows();
}

void Workspace::deleteSequence(Sequence *sequence)
{
    beginRemoveRows(createIndex(sequence->parent()->row(), 0, sequence->parent()), sequence->row(), sequence->row());
    sequence->project()->deleteSequence(sequence);
    endRemoveRows();
}

void Workspace::deleteTrack(Track *track)
{
    beginRemoveRows(createIndex(track->parent()->row(), 0, track->parent()), track->row(), track->row());
    track->sequence()->deleteTrack(track);
    endRemoveRows();
}

void Workspace::deleteReceiver(Receiver *receiver)
{
    beginRemoveRows(createIndex(receiver->parent()->row(), 0, receiver->parent()), receiver->row(), receiver->row());
    receiver->track()->deleteReceiver(receiver);
    endRemoveRows();
}

void Workspace::renameProject(Project *project, QString name)
{
    project->setName(name);
    emit dataChanged(createIndex(project->row(), 0, project),createIndex(project->row(), 0, project));
}

void Workspace::renameSequence(Sequence *sequence, QString name)
{
    sequence->setName(name);
    emit dataChanged(createIndex(sequence->row(), 0, sequence),createIndex(sequence->row(), 0, sequence));
}

void Workspace::renameTrack(Track *track, QString name)
{
    track->setName(name);
    emit dataChanged(createIndex(track->row(), 0, track),createIndex(track->row(), 0, track));

}

void Workspace::moveSequence(Project *parent, int from, int to)
{
    beginMoveRows(createIndex(parent->row(), 0, parent), from, from, createIndex(parent->row(), 0, parent), (from+1==to)?to+1:to);
    parent->moveSequence(from, to);
    endMoveRows();
}

QVariant Workspace::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

    if (role == Qt::DecorationRole)
    {
        QPixmap icon;
        switch (item->itemType()) {
        case 0:
            icon = QPixmap(":projectIcon").scaled(32, 32, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            break;
        case 1:
            icon = QPixmap(":sequenceIcon").scaled(32, 32, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            break;
        case 2:
            icon = QPixmap(":trackIcon").scaled(32, 32, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            break;
        case 3:
            icon = QPixmap(":receiverIcon").scaled(24, 24, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            break;
        default:
            break;
        }
        return icon;
    }

    if (role != Qt::DisplayRole)
        return QVariant();

    return item->data(index.column());
}

Qt::ItemFlags Workspace::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant Workspace::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section);
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return "root";

    return QVariant();
}
