#include "trackfactory.h"
#include <QDir>
#include "application.h"
#include "trackplugin.h"

#include <QMessageBox>
#include <QDebug>

TrackFactory::TrackFactory(QObject *parent) : QObject(parent)
{
    QDir plugDir = QDir(qApp->applicationDirPath());

    if(plugDir.cd("./TrackPlugins")) {

        foreach(QString file, plugDir.entryList(QDir::Files)) {
            QPluginLoader loader(plugDir.absoluteFilePath(file));

            if(QObject *plugin = loader.instance()) {

                TrackPlugin *trackPlugin = qobject_cast<TrackPlugin *>(plugin);
                m_plugins.insert(trackPlugin->type(), trackPlugin);

            }
            else {
                QMessageBox messageBox;
                messageBox.setWindowTitle(tr("Echec du chargement d'un plugin"));
                messageBox.setText(tr("Une erreur s'est produite lors du chargement d'un plugin\n") +
                                   tr("Certaines fonctions ne seront pas disponnibles."));
                messageBox.setDetailedText(loader.errorString());
                messageBox.setIcon(QMessageBox::Warning);
                messageBox.exec();

            }

        }
    }

}

TrackFactory::~TrackFactory()
{

}

TrackPlugin *TrackFactory::pluginInstance(QString type)
{
    return m_plugins.value(type, nullptr);
}

