#ifndef NEWPROJECTDIALOG_H
#define NEWPROJECTDIALOG_H

#include <QDialog>
#include "application.h"

namespace Ui {
class NewProjectDialog;
}

class NewProjectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewProjectDialog(Application *app, QWidget *parent = 0);
    ~NewProjectDialog();

private slots:


    void on_browsePushButton_clicked();

    void on_buttonBox_accepted();

private:
    Application *m_app;
    Ui::NewProjectDialog *ui;
};

#endif // NEWPROJECTDIALOG_H
