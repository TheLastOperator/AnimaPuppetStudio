#ifndef TRACKWIDGET
#define TRACKWIDGET

#include <QWidget>
#include <QMenu>

#include "../models/trackdata.h"

class TrackWidget : public QWidget
{
    Q_OBJECT
public:
    TrackWidget(QWidget *parent = 0) : QWidget(parent) {}
    virtual void setTimeCursor(qint64 time) = 0;
    virtual QMenu *configMenu() = 0;
};

#endif // TRACKWIDGET

