#include "helpdialog.h"
#include "ui_helpdialog.h"

#include <QHelpEngine>
#include <QHelpContentWidget>
#include <QHelpIndexWidget>
#include <QSplitter>

#include <QDebug>

#include <QTextBrowser>

#include "helpbrowser.h"

HelpDialog::HelpDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HelpDialog)
{
    ui->setupUi(this);

    qDebug() << QApplication::applicationDirPath() +
                "/documentation/animapuppetstudiohelp.qhc";

    QHelpEngine* helpEngine = new QHelpEngine(
                    QApplication::applicationDirPath() +
                    "/documentation/animapuppetstudiohelp.qhc");
    helpEngine->setupData();

    QHelpContentWidget *helpContentWidget = helpEngine->contentWidget();
    helpContentWidget->setExpandsOnDoubleClick(false);

    ui->tabWidget->addTab(helpContentWidget, "Sommaire");
    ui->tabWidget->addTab(helpEngine->indexWidget(), "Index");

    HelpBrowser *textViewer = new HelpBrowser(helpEngine, this);

    QString sheet = "a { text-decoration: underline; color: rgb(221, 228, 86); }";
    textViewer->document()->setDefaultStyleSheet(sheet);

    textViewer->setSource(
                QUrl("qthelp://AnimaPuppetStudio.help/doc/index.html"));
    connect(helpEngine->contentWidget(),
            SIGNAL(linkActivated(QUrl)),
            textViewer, SLOT(setSource(QUrl)));

    connect(helpEngine->indexWidget(),
            SIGNAL(linkActivated(QUrl, QString)),
            textViewer, SLOT(setSource(QUrl)));

    ui->widget->layout()->addWidget(textViewer);
}

HelpDialog::~HelpDialog()
{
    delete ui;
}
