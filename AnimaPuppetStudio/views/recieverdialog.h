#ifndef RECIEVERDIALOG_H
#define RECIEVERDIALOG_H

#include "models/receiver.h"

#include <QDialog>

namespace Ui {
class RecieverDialog;
}

class ReceiverDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ReceiverDialog(Receiver *receiver, QWidget *parent = 0);
    ~ReceiverDialog();

private slots:
    void saveReceiver();

private:
    Ui::RecieverDialog *ui;
    Receiver *m_receiver;
};

#endif // RECIEVERDIALOG_H
