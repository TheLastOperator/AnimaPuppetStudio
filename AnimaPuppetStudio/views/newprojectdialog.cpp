#include "newprojectdialog.h"
#include "ui_newprojectdialog.h"

#include "filemanager.h"

#include <QFileDialog>

NewProjectDialog::NewProjectDialog(Application *app, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewProjectDialog)
{
    ui->setupUi(this);
    m_app = app;
    ui->pathLineEdit->setText(m_app->settings()->value("workspacePath").value<QString>());
}

NewProjectDialog::~NewProjectDialog()
{
    delete ui;
}


void NewProjectDialog::on_browsePushButton_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Emplacement du projet"),
                                                    m_app->settings()->value("workspacePath").value<QString>(),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    ui->pathLineEdit->setText(dir);
}

void NewProjectDialog::on_buttonBox_accepted()
{
    m_app->fileManager()->newProject(ui->nameLineEdit->text(), ui->pathLineEdit->text());
}
