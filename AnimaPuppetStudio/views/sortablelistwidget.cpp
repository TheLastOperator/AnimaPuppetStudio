#include "sortablelistwidget.h"

#include <QPainter>
#include <QMouseEvent>

#include "application.h"

SortableListWidget::SortableListWidget(Project *project, QWidget *parent) : QWidget(parent)
{
    m_viewOffset = 0;
    m_spacing = 24;
    m_currentSelection = -1;
    m_dropIndex = -1;
    m_playIndex = 0;

    m_project = project;
}

SortableListWidget::~SortableListWidget()
{

}

void SortableListWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.fillRect(rect(), QBrush(QColor("#000000"/*"#303234"*/)));

    QPen penHLinesWhite(QColor("#FFFFFF"), 2);
    QPen penHLinesCurveGreen(QColor("#89FF1D"), 2);
    QPen penHLinesCurveRed(QColor("#FF891D"), 2);

    for (int i = m_viewOffset; i < m_viewOffset+height()/m_spacing+1; ++i) {
        if(i == m_dropIndex) {
            painter.setPen(penHLinesCurveRed);
            painter.drawLine(8,8+i*m_spacing,width()-16,8+i*m_spacing);
        }
        if(m_project->sequences().size() > i) {
            painter.setPen((i == m_currentSelection)?penHLinesCurveRed:(i == m_playIndex)?penHLinesCurveGreen:penHLinesWhite);
            painter.drawText(16,14+i*m_spacing,width()-32,m_spacing,0,m_project->sequences().at(i)->name());
        }
    }
}

void SortableListWidget::mousePressEvent(QMouseEvent *event)
{
    int index = m_viewOffset+(event->pos().y()-8)/m_spacing;

    if(event->button() == Qt::LeftButton) {
        if(m_project->sequences().size() > index) {
            m_currentSelection = index;
        }
        else {
            m_currentSelection = -1;
        }
    }

    update();
}

void SortableListWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(m_currentSelection != -1) {
        int drop = m_viewOffset+(event->pos().y()+8)/m_spacing;
        if(m_project->sequences().size() >= drop) {
            m_dropIndex = drop;
        }
        update();
    }
}

void SortableListWidget::mouseReleaseEvent(QMouseEvent *)
{
    if(m_currentSelection != -1 && m_dropIndex != -1 && m_currentSelection != m_dropIndex && (m_dropIndex != m_currentSelection + 1)) {
        Application::instance()->workspace()->moveSequence(m_project, m_currentSelection, (m_dropIndex > m_currentSelection)?m_dropIndex-1:m_dropIndex);
        //itemList.insert((dropIndex > currentSelection)?dropIndex-1:dropIndex,itemList.takeAt(currentSelection));
    }
    m_currentSelection = -1;
    m_dropIndex = -1;
    update();
}
