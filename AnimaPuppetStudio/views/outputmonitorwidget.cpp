#include "outputmonitorwidget.h"
#include <QPainter>

OutputMonitorWidget::OutputMonitorWidget(QWidget *parent) : QWidget(parent)
{

}

OutputMonitorWidget::~OutputMonitorWidget()
{
    qDeleteAll(m_history);
}

void OutputMonitorWidget::insertData(int index, qint8 data)
{
    if(index >= m_history.size()) {
        m_history.append(new QList<qint8>);
    }

    while (m_history.at(index)->size() <= width()/8) {
        m_history.at(index)->append(0);
    }

    m_history.at(index)->append(data);

    if (m_history.at(index)->size() > width()/8 +1) {
        m_history.at(index)->removeFirst();
    }

    update();
}

/**
 * @brief Do the painting
 * @param event QPaint event
 */
void OutputMonitorWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.fillRect(rect(), QBrush(QColor("#000000"/*"#303234"*/)));

    QPen penHLinesRed(QColor("#FF8010"), 2);

    int spacing = 8;

    for (int j = 0; j < m_history.size(); ++j) {
        for (int i = 0; i < m_history.at(j)->size(); ++i) {
            painter.setPen(penHLinesRed);
            painter.setBrush(QColor("#FF8010"));
            qint8 value = m_history.at(j)->at(i)/2;
            painter.drawRect(i*spacing, j*70+4 + 64-value, 4, value);
        }
    }
    /*
    for (int j = 0; j < height()/64+1; ++j) {
        for (int i = 0; i < width()/spacing+1; ++i) {
            painter.setPen(penHLinesRed);
            painter.setBrush(QColor("#FF8010"));
            int value = (qrand() % 65);
            painter.drawRect(i*spacing, j*70+4 + value, 4, 64 - value);
        }
    }*/
}
