#include "sequenceform.h"
#include "ui_sequenceform.h"

#include "models/project.h"

#include "views/trackform.h"

#include <QFileDialog>
#include <QInputDialog>

#include "application.h"

SequenceForm::SequenceForm(Sequence *sequence, QWidget *parent) :
    MdiChild(parent),
    ui(new Ui::SequenceForm)
{
    m_sequence = sequence;
    m_sequenceDriver = new SequenceDriver(m_sequence);
    ui->setupUi(this);
    setWindowTitle(m_sequence->name());
    ui->projectNameLabel->setText(m_sequence->project()->name());
    ui->sequenceNameLabel->setText(m_sequence->name());
    ui->audioPathLabel->setText(m_sequence->audioPath());

    connect(m_sequence, &Sequence::destroyed, this, &SequenceForm::closeView );
    connect(m_sequence, SIGNAL(renamed(QString)), ui->sequenceNameLabel, SLOT(setText(QString)));
    connect(m_sequence->project(), SIGNAL(renamed(QString)), ui->projectNameLabel, SLOT(setText(QString)));
    connect(m_sequence, SIGNAL(renamed(QString)), this, SLOT(setWindowTitle(QString)));
    connect(m_sequence, SIGNAL(audioPathChanged(QString)), ui->audioPathLabel, SLOT(setText(QString)));
    connect(ui->playButton, SIGNAL(clicked()), m_sequenceDriver, SLOT(togglePlay()));
    connect(ui->stopButton, SIGNAL(clicked()), m_sequenceDriver, SLOT(stop()));
    connect(m_sequenceDriver, &SequenceDriver::durationChanged, this, &SequenceForm::updateDuration);
    connect(m_sequenceDriver, &SequenceDriver::positionChanged, this, &SequenceForm::updatePosition);
    connect(ui->progressSlider, &QSlider::valueChanged, m_sequenceDriver, &SequenceDriver::setPosition);
    connect(ui->volumeSlider, &QSlider::valueChanged, m_sequenceDriver, &SequenceDriver::setVolume);

    connect(ui->chooseMusicButton, &QPushButton::clicked, this, &SequenceForm::chooseMusic);
    connect(ui->addTrackButton, &QPushButton::clicked, this, &SequenceForm::addTrack);
    connect(m_sequence, &Sequence::trackAdded, this, &SequenceForm::loadTracks);

    loadTracks();
}

SequenceForm::~SequenceForm()
{
    delete ui;
    delete m_sequenceDriver;
}

TreeItem *SequenceForm::treeItem()
{
    return m_sequence;
}

void SequenceForm::closeView()
{
    parentWidget()->close();
    close();
    delete this;
}

void SequenceForm::updateDuration(qint64 duration)
{
    ui->progressSlider->setMaximum(duration);
    ui->maxTimingLabel->setText(QString("%1:%2:%3")
                                .arg(duration / 60000, 2, 10, QChar('0'))
                                .arg(duration / 1000 % 60, 2, 10, QChar('0'))
                                .arg(duration % 1000, 3, 10, QChar('0')));
}

void SequenceForm::updatePosition(qint64 position)
{
    bool state = ui->progressSlider->blockSignals(true);
    ui->progressSlider->setValue(position);
    ui->progressSlider->blockSignals(state);
    ui->currentTimingLabel->setText(QString("%1:%2:%3")
                                .arg(position / 60000, 2, 10, QChar('0'))
                                .arg(position / 1000 % 60, 2, 10, QChar('0'))
                                .arg(position % 1000, 3, 10, QChar('0')));
}

void SequenceForm::chooseMusic()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Choix de la piste audio"), m_sequence->project()->path(), tr("Piste audio (*.wav)"));
    QDir dir(m_sequence->project()->path());
    dir.cd("..");
    m_sequence->setAudioPath(dir.relativeFilePath(fileName));
}

void SequenceForm::addTrack()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("Nouvelle piste"),
                                         tr("Nom :"), QLineEdit::Normal,
                                         tr("Piste"), &ok);
    if (ok && !name.isEmpty()) {
        Application::instance()->workspace()->addTrack(m_sequence, name, "AnalogTrack");
    }
}

void SequenceForm::loadTracks()
{
    QLayoutItem *item;
    while((item = ui->tracksWidget->layout()->takeAt(0))) {
        if (item->layout()) {
            delete item->layout();
        }
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }
    for(Track *track : m_sequence->tracks()) {
        TrackForm *trackForm = new TrackForm(track, ui->tracksScrollArea->widget());
        connect(m_sequenceDriver, &SequenceDriver::positionChanged, trackForm, &TrackForm::driverPositionUpdated);
        ui->tracksWidget->layout()->addWidget(trackForm);
        trackForm->driverPositionUpdated(m_sequenceDriver->position());
    }
}
