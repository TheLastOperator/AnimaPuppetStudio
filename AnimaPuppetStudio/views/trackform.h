#ifndef TRACKFORM_H
#define TRACKFORM_H

#include <QWidget>

#include "models/track.h"

namespace Ui {
class TrackForm;
}

class TrackForm : public QWidget
{
    Q_OBJECT

public:
    explicit TrackForm(Track *track, QWidget *parent = 0);
    ~TrackForm();

signals:
    void driverPositionUpdated(qint64);

public slots:
    void setDriverPosition(qint64 position);

private slots:
    void on_deletePushButton_clicked();

private:
    Ui::TrackForm *ui;
    Track *m_track;
};

#endif // TRACKFORM_H
