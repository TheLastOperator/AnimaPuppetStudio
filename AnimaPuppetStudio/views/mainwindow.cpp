#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "application.h"

#include <QMdiArea>
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QSerialPortInfo>

#include "models/workspace.h"
#include "models/project.h"

#include "views/projectform.h"
#include "views/sequenceform.h"
#include "views/mdichild.h"
#include "views/newprojectdialog.h"
#include "views/recieverdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->workspaceTreeView->setModel(Application::instance()->workspace());
    ui->workspaceTreeView->expandToDepth(1);
    ui->workspaceTreeView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->workspaceTreeView, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(openWorkspaceTreeContextMenu(const QPoint &)));
    connect(ui->workspaceTreeView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(openTreeItem(QModelIndex)));
    connect(Application::instance(), &Application::showUp, this, &MainWindow::activateWindow );
    connect(Application::instance()->workspace(), &Workspace::rowsInserted, ui->workspaceTreeView, &QTreeView::expand);
    connect(ui->serialComboBox, &QComboBox::currentTextChanged,Application::instance()->serialBridge(), &SerialBridge::open);

    refreshSerialPortsList();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openTreeItem(QModelIndex modelIndex)
{
    TreeItem *item = static_cast<TreeItem*>(modelIndex.internalPointer());
    foreach (QMdiSubWindow *window, ui->mdiArea->subWindowList()) {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
        if (mdiChild->treeItem() == item) {
            ui->mdiArea->setActiveSubWindow(window);
            return;
        }
    }
    switch (item->itemType()) {
        case 0:
        {
            // Project
            ProjectForm *projectForm = new ProjectForm(static_cast<Project*>(item));
            ui->mdiArea->addSubWindow(projectForm);
            projectForm->show();
            projectForm->activateWindow();
            break;
        }
        case 1:
        {
            // Sequence
            SequenceForm *sequenceForm = new SequenceForm(static_cast<Sequence*>(item));
            ui->mdiArea->addSubWindow(sequenceForm);
            sequenceForm->show();
            sequenceForm->activateWindow();
            break;
        }
        case 3:
        {
            // Receiver
            ReceiverDialog dialog(static_cast<Receiver*>(item), this);
            dialog.exec();
            break;
        }
    }
}

void MainWindow::printToLogs(QString log)
{
    ui->logs->appendPlainText(log);
}

void MainWindow::openWorkspaceTreeContextMenu(const QPoint &point)
{
    QModelIndex index = ui->workspaceTreeView->indexAt(point);
    if (index.isValid()) {
        TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
        switch (item->itemType()) {
            case 0: {
                // Project
                QMenu contextMenu;
                QAction* addSequenceAction = contextMenu.addAction("Ajouter une séquence");
                QAction* importSequenceAction = contextMenu.addAction("Importer une séquence");
                QAction* saveProjectAction = contextMenu.addAction("Sauvegarder le projet");
                QAction* renameProjectAction = contextMenu.addAction("Renommer le projet");
                QAction* closeProjectAction = contextMenu.addAction("Fermer le projet");

                QAction* selectedItem = contextMenu.exec(ui->workspaceTreeView->mapToGlobal(point));
                if (selectedItem == closeProjectAction) {
                    Application::instance()->workspace()->deleteProject(static_cast<Project*>(item));
                }
                else if (selectedItem == addSequenceAction) {
                    bool ok;
                    QString name = QInputDialog::getText(this, tr("Nouvelle séquence"),
                                                         tr("Nom :"), QLineEdit::Normal,
                                                         "Sequence", &ok);
                    if (ok && !name.isEmpty())
                        Application::instance()->workspace()->addSequence(static_cast<Project*>(item), name);
                }
                else if (selectedItem == renameProjectAction) {
                    bool ok;
                    QString name = QInputDialog::getText(this, tr("Renommer le projet"),
                                                         tr("Nom :"), QLineEdit::Normal,
                                                         item->name(), &ok);
                    if (ok && !name.isEmpty())
                        item->setName(name);
                }
                else if (selectedItem == saveProjectAction) {
                    Application::instance()->fileManager()->saveProject(static_cast<Project*>(item));
                }
                else if (selectedItem == importSequenceAction) {
                    QString fileName = QFileDialog::getOpenFileName(this, tr("Ouvrir un projet"), Application::instance()->settings()->value("workspacePath").value<QString>() + "/", tr("Projet Anima Puppet Studio (*.anima)"));
                    FileManager::Error error;
                    Application::instance()->fileManager()->importOldSequenceFormat(fileName, static_cast<Project*>(item), error);
                    QMessageBox msgBox;
                    switch (error) {
                    case FileManager::CANT_OPEN_FILE:
                        msgBox.setIcon(QMessageBox::Critical);
                        msgBox.setWindowTitle(tr("Impossible d'importer la sequence"));
                        msgBox.setText("Impossible de lire le fichier.");
                        msgBox.exec();
                        break;
                    case FileManager::INVALID_FILE_FORMAT:
                        msgBox.setIcon(QMessageBox::Critical);
                        msgBox.setWindowTitle(tr("Impossible d'importer la sequence"));
                        msgBox.setText("Le fichier est endomagé ou incompatible avec cette version.");
                        msgBox.exec();
                        break;
                    case FileManager::OLD_FILE_VERSION:
                        msgBox.setIcon(QMessageBox::Warning);
                        msgBox.setWindowTitle(tr("Impossible d'importer la sequence"));
                        msgBox.setText("Le fichier a été conçu avec une version du logiciel qui n'est plus supportée.");
                        msgBox.exec();
                        break;
                    case FileManager::NEW_FILE_VERSION:
                        msgBox.setIcon(QMessageBox::Warning);
                        msgBox.setWindowTitle(tr("Impossible d'importer la sequence"));
                        msgBox.setText("Le fichier a été conçu avec une version du logiciel plus récente.");
                        msgBox.exec();
                        break;
                    default:
                        break;
                    }
                }
                break;
            }
            case 1: {
                // Sequence
                QMenu contextMenu;
                QAction* addTrackAction = contextMenu.addAction("Ajouter une piste");
                QAction* renameSequenceAction = contextMenu.addAction("Renommer la séquence");
                QAction* deleteSequenceAction = contextMenu.addAction("Supprimer la séquence");

                QAction* selectedItem = contextMenu.exec(ui->workspaceTreeView->mapToGlobal(point));
                if(selectedItem == addTrackAction) {
                    bool ok;
                    QString name = QInputDialog::getText(this, tr("Nouvelle piste"),
                                                         tr("Nom :"), QLineEdit::Normal,
                                                         tr("Piste"), &ok);
                    if (ok && !name.isEmpty()) {
                        static_cast<Application *>(QApplication::instance())->workspace()->addTrack(static_cast<Sequence*>(item), name, "AnalogTrack");
                    }
                }
                else if(selectedItem == deleteSequenceAction) {
                    Application::instance()->workspace()->deleteSequence(static_cast<Sequence*>(item));
                }
                else if (selectedItem == renameSequenceAction) {
                    bool ok;
                    QString name = QInputDialog::getText(this, tr("Renommer la séquence"),
                                                         tr("Nom :"), QLineEdit::Normal,
                                                         item->name(), &ok);
                    if (ok && !name.isEmpty())
                        item->setName(name);
                }
                break;
            }
            case 2: {
                // Track
                QMenu contextMenu;
                QAction* addReceiverkAction = contextMenu.addAction("Ajouter un recepteur");
                QAction* renameTrackAction = contextMenu.addAction("Renommer la piste");
                QAction* deleteTrackAction = contextMenu.addAction("Supprimer la piste");

                QAction* selectedItem = contextMenu.exec(ui->workspaceTreeView->mapToGlobal(point));
                if(selectedItem == addReceiverkAction) {
                    Receiver *receiver = new Receiver(static_cast<Track*>(item));
                    receiver->setName("Récepteur");
                    Application::instance()->workspace()->addReceiver(static_cast<Track*>(item), receiver);
                    ReceiverDialog dialog(receiver, this);
                    dialog.exec();
                }
                else if(selectedItem == deleteTrackAction) {
                    Application::instance()->workspace()->deleteTrack(static_cast<Track*>(item));
                }
                else if (selectedItem == renameTrackAction) {
                    bool ok;
                    QString name = QInputDialog::getText(this, tr("Renommer la piste"),
                                                         tr("Nom :"), QLineEdit::Normal,
                                                         item->name(), &ok);
                    if (ok && !name.isEmpty())
                        item->setName(name);
                }
                break;
            }
            case 3: {
                // Receiver
                QMenu contextMenu;
                QAction* editReceiverAction = contextMenu.addAction("Modifier le récepteur");
                QAction* deleteReceiverAction = contextMenu.addAction("Supprimer le récepteur");

                QAction* selectedItem = contextMenu.exec(ui->workspaceTreeView->mapToGlobal(point));
                if(selectedItem == deleteReceiverAction) {
                    Application::instance()->workspace()->deleteReceiver(static_cast<Receiver*>(item));
                }
                else if (selectedItem == editReceiverAction) {
                    ReceiverDialog dialog(static_cast<Receiver*>(item), this);
                    dialog.exec();
                }
                break;
            }
        }
    }
}

void MainWindow::on_actionNewProject_triggered()
{
    NewProjectDialog dialog(Application::instance());
    dialog.exec();

}

void MainWindow::on_actionOpenProject_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Ouvrir un projet"), Application::instance()->settings()->value("workspacePath").value<QString>() + "/", tr("Projet Anima Puppet Studio (*.anima)"));
    FileManager::Error error;
    Application::instance()->fileManager()->openProject(fileName, error);
    QMessageBox msgBox;
    switch (error) {
    case FileManager::CANT_OPEN_FILE:
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(tr("Impossible d'ouvrir le projet"));
        msgBox.setText("Impossible de lire le fichier.");
        msgBox.exec();
        break;
    case FileManager::INVALID_FILE_FORMAT:
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(tr("Impossible d'ouvrir le projet"));
        msgBox.setText("Le fichier est endomagé ou incompatible avec cette version.");
        msgBox.exec();
        break;
    case FileManager::OLD_FILE_VERSION:
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(tr("Impossible d'ouvrir le projet"));
        msgBox.setText("Le fichier a été conçu avec une version du logiciel qui n'est plus supportée.");
        msgBox.exec();
        break;
    case FileManager::NEW_FILE_VERSION:
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(tr("Impossible d'ouvrir le projet"));
        msgBox.setText("Le fichier a été conçu avec une version du logiciel plus récente.");
        msgBox.exec();
        break;
    default:
        break;
    }
}

void MainWindow::on_actionSaveAllProjects_triggered()
{
    for (Project *project : Application::instance()->workspace()->projects()) {
        Application::instance()->fileManager()->saveProject(project);
    }
}

void MainWindow::refreshSerialPortsList()
{
    ui->serialComboBox->clear();
    QString description;
    QString manufacturer;
    QString serialNumber;
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        ui->serialComboBox->addItem(info.portName());
    }

    //ui->serialComboBox->addItem(tr("Custom"));

}

void MainWindow::on_pushButton_clicked()
{
    refreshSerialPortsList();
}

void MainWindow::on_actionHelp_triggered()
{
    m_helpDialog.show();
}
