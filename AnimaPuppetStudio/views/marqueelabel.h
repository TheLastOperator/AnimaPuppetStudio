#ifndef MARQUEELABEL_H
#define MARQUEELABEL_H


#include <QLabel>
#include <QStaticText>
#include <QTimer>


class MarqueeLabel : public QLabel
{
    Q_OBJECT

public:
    explicit MarqueeLabel(QWidget *parent = 0);

public slots:
    void setText(QString text);

    QString separator() const;
    void setSeparator(QString separator);


protected:
    virtual void paintEvent(QPaintEvent *);
    virtual void resizeEvent(QResizeEvent *);

private:
    void updateText();
    QString _separator;
    QStaticText staticText;
    int singleTextWidth;
    QSize wholeTextSize;
    int leftMargin;
    bool scrollEnabled;
    int scrollPos;
    QImage alphaChannel;
    QImage buffer;
    QTimer timer;

private slots:
    virtual void timer_timeout();
};

#endif // MARQUEELABEL_H
