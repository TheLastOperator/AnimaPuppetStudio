#include "trackform.h"
#include "ui_trackform.h"

#include "application.h"

#include "models/trackdata.h"
#include "views/trackwidget.h"

#include <QMessageBox>

TrackForm::TrackForm(Track *track, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TrackForm)
{
    ui->setupUi(this);
    m_track = track;

    ui->trackNameLabel->setText(m_track->name());

    connect(m_track, &Track::renamed, ui->trackNameLabel, &QLabel::setText);   
    connect(m_track, &Track::destroyed, this, &TrackForm::deleteLater);

    if(m_track->trackData() != nullptr) {
        TrackPlugin *trackPlugin = Application::instance()->trackFactory()->pluginInstance(track->trackData()->type());
        if(trackPlugin != nullptr) {
            TrackWidget *trackWidget = trackPlugin->bindWidget(m_track->trackData());
            trackWidget->setParent(this);
            ui->configPushButton->setMenu(trackWidget->configMenu());
            connect(this, &TrackForm::driverPositionUpdated, trackWidget, &TrackWidget::setTimeCursor);
            ui->widget->layout()->addWidget(trackWidget);
        }
    }
}

TrackForm::~TrackForm()
{
    delete ui;
}

void TrackForm::setDriverPosition(qint64 position)
{
    emit driverPositionUpdated(position);
}

void TrackForm::on_deletePushButton_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, tr("Supprimer la piste ?"), "Voullez vous vraiment supprimer cette piste ?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        Application::instance()->workspace()->deleteTrack(m_track);
    }

}
