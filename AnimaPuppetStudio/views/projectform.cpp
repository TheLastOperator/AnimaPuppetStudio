#include "projectform.h"
#include "ui_projectform.h"

#include "sortablelistwidget.h"

#include "application.h"

ProjectForm::ProjectForm(Project *project, QWidget *parent) :
    MdiChild(parent),
    ui(new Ui::ProjectForm)
{
    m_project = project;
    m_sequenceDriver = nullptr;
    ui->setupUi(this);
    setWindowTitle(m_project->name());
    ui->projectNameLabel->setText(m_project->name());
    connect(m_project, &Sequence::destroyed, this, &ProjectForm::closeView );
    //connect(m_project, SIGNAL(destroyed()), this, SLOT(close()));
    connect(m_project, SIGNAL(renamed(QString)), ui->projectNameLabel, SLOT(setText(QString)));
    connect(m_project, SIGNAL(renamed(QString)), this, SLOT(setWindowTitle(QString)));

    playlistIndex = -1;
    nextSequence();

    ui->sequencesPlaylistWidget->layout()->addWidget(new SortableListWidget(m_project, this));
}

ProjectForm::~ProjectForm()
{
    delete ui;
    delete m_sequenceDriver;
}

TreeItem *ProjectForm::treeItem()
{
    return m_project;
}

void ProjectForm::loadSequence(Sequence *sequence)
{
    ui->progressSlider->setValue(0); // Fix a crash when moving the slider while loading the next song

    if(m_sequenceDriver != nullptr) {
        // This part disconnect the previous sequence as it must be linked only to the current sequence and nothing else.
        disconnect(m_sequenceDriver->sequence(), &Sequence::renamed, ui->sequenceNameLabel, &QLabel::setText);
        disconnect(m_sequenceDriver->sequence(), &Sequence::audioPathChanged, ui->audioPathLabel, &QLabel::setText);
        //
        m_sequenceDriver->deleteLater();
    }
    m_sequenceDriver = new SequenceDriver(sequence);

    ui->sequenceNameLabel->setText(sequence->name());
    ui->audioPathLabel->setText(sequence->audioPath());

    //connect(sequence, &Sequence::destroyed, m_sequenceDriver, &SequenceDriver::deleteLater );
    connect(sequence, &Sequence::destroyed, this, &ProjectForm::nextSequence );

    // Warning ! This part must be disconnected as it must be linked only to the current sequence and nothing else.
    connect(sequence, &Sequence::renamed,          ui->sequenceNameLabel, &QLabel::setText);
    connect(sequence, &Sequence::audioPathChanged, ui->audioPathLabel, &QLabel::setText);
    //

    connect(ui->playButton, SIGNAL(clicked()), m_sequenceDriver, SLOT(togglePlay()));
    connect(ui->stopButton, SIGNAL(clicked()), m_sequenceDriver, SLOT(stop()));

    connect(m_sequenceDriver, &SequenceDriver::durationChanged, this, &ProjectForm::updateDuration);
    connect(m_sequenceDriver, &SequenceDriver::positionChanged, this, &ProjectForm::updatePosition);
    connect(m_sequenceDriver, &SequenceDriver::finished,        this, &ProjectForm::nextSequence);

    connect(m_sequenceDriver, &SequenceDriver::trackUpdated,    ui->outputMonitorWidget, &OutputMonitorWidget::insertData);

    connect(ui->progressSlider, &QSlider::valueChanged, m_sequenceDriver, &SequenceDriver::setPosition);
    connect(ui->volumeSlider,   &QSlider::valueChanged, m_sequenceDriver, &SequenceDriver::setVolume);
}


void ProjectForm::closeView()
{
    parentWidget()->close();
    close();
    delete this;
}


void ProjectForm::updateDuration(qint64 duration)
{
    ui->progressSlider->setMaximum(duration);
    ui->maxTimingLabel->setText(QString("%1:%2:%3")
                                .arg(duration/600, 2, 10, QChar('0'))
                                .arg(duration/10%60, 2, 10, QChar('0'))
                                .arg(duration%10, 2, 10, QChar('0')));
}

void ProjectForm::updatePosition(qint64 position)
{
    bool state = ui->progressSlider->blockSignals(true);
    ui->progressSlider->setValue(position);
    ui->progressSlider->blockSignals(state);
    ui->currentTimingLabel->setText(QString("%1:%2:%3")
                                .arg(position/600, 2, 10, QChar('0'))
                                .arg(position/10%60, 2, 10, QChar('0'))
                                    .arg(position%10, 2, 10, QChar('0')));
}

void ProjectForm::nextSequence()
{
    if(playlistIndex+1 < m_project->sequences().size()) {
        playlistIndex++;
    }
    else {
        playlistIndex = 0;
    }
    if(playlistIndex < m_project->sequences().size()) {
        loadSequence(m_project->sequences().at(playlistIndex));
    }
    else {
        playlistIndex = -1;
        ui->progressSlider->setValue(0); // Fix a crash when moving the slider while loading the next song

        if(m_sequenceDriver != nullptr) {
            // This part disconnect the previous sequence as it must be linked only to the current sequence and nothing else.
            disconnect(m_sequenceDriver->sequence(), &Sequence::renamed, ui->sequenceNameLabel, &QLabel::setText);
            disconnect(m_sequenceDriver->sequence(), &Sequence::audioPathChanged, ui->audioPathLabel, &QLabel::setText);
            //
            m_sequenceDriver->deleteLater();
        }

        m_sequenceDriver = nullptr;
        ui->sequenceNameLabel->setText("-");
        ui->audioPathLabel->setText("-");
    }
    ui->currentSequenceIndexLabel->setText(QString::number(playlistIndex+1));
    ui->maxSequenceIndexLabel->setText(QString::number(m_project->sequences().size()));

}
