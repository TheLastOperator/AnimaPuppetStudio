#ifndef PROJECTFORM_H
#define PROJECTFORM_H

#include <QWidget>

#include "mdichild.h"
#include "sequencedriver.h"
#include "models/project.h"
#include "models/treeitem.h"

namespace Ui {
class ProjectForm;
}

class ProjectForm : public MdiChild
{
public:
    explicit ProjectForm(Project *project, QWidget *parent = 0);
    ~ProjectForm();

    TreeItem *treeItem();

public slots:
    void loadSequence(Sequence *sequence);
    void closeView();
    void updateDuration(qint64 duration);
    void updatePosition(qint64 position);
    void nextSequence();

private:
    Ui::ProjectForm *ui;
    Project *m_project;
    SequenceDriver *m_sequenceDriver;
    int playlistIndex;
};

#endif // PROJECTFORM_H
