#include "recieverdialog.h"
#include "ui_recieverdialog.h"

ReceiverDialog::ReceiverDialog(Receiver *receiver, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RecieverDialog)
{
    ui->setupUi(this);
    setWindowTitle(tr("Modifier le récepteur"));
    m_receiver = receiver;
    ui->nameLineEdit->setText(m_receiver->name());
    ui->receiverIdLineEdit->setText(QString("%1").arg(m_receiver->uuid(), 16, 16, QChar('0')));
    ui->pinOutIdSpinBox->setValue(m_receiver->port());
    ui->invertedCheckBox->setChecked(m_receiver->inverted());
    connect(this, &QDialog::accepted, this, &ReceiverDialog::saveReceiver);
    connect(m_receiver, &Receiver::destroyed, this, &ReceiverDialog::deleteLater);
}

ReceiverDialog::~ReceiverDialog()
{
    delete ui;
}

void ReceiverDialog::saveReceiver()
{
    bool ok;
    m_receiver->setName(ui->nameLineEdit->text());
    m_receiver->setUuid(ui->receiverIdLineEdit->text().toULongLong(&ok, 16));
    m_receiver->setPort(ui->pinOutIdSpinBox->value());
    m_receiver->setInverted(ui->invertedCheckBox->isChecked());
}
