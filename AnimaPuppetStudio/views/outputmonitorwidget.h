#ifndef OUTPUTMONITORWIDGET_H
#define OUTPUTMONITORWIDGET_H

#include <QWidget>
#include <QList>

class OutputMonitorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit OutputMonitorWidget(QWidget *parent = 0);
    ~OutputMonitorWidget();

public slots:
    void insertData(int index, qint8 data);

protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;

private:
    QList<QList<qint8>*> m_history;
};

#endif // OUTPUTMONITORWIDGET_H
