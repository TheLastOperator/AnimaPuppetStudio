#ifndef SORTABLELISTWIDGET_H
#define SORTABLELISTWIDGET_H

#include <QWidget>
#include "models/project.h"

class SortableListWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SortableListWidget(Project *project, QWidget *parent = 0);
    ~SortableListWidget();
protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void mousePressEvent (QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent (QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent (QMouseEvent *) Q_DECL_OVERRIDE;
private:
    Project *m_project;
    int m_viewOffset;
    int m_spacing;
    int m_currentSelection;
    int m_dropIndex;
    int m_playIndex;
};

#endif // SORTABLELISTWIDGET_H
