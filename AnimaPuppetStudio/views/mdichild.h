#ifndef MDICHILD_H
#define MDICHILD_H

#include <QWidget>

#include "models/treeitem.h"

class MdiChild : public QWidget
{
    Q_OBJECT
public:
    MdiChild(QWidget *parent = 0);
    ~MdiChild();

    virtual TreeItem *treeItem() = 0;

signals:

public slots:

};

#endif // MDICHILD_H
