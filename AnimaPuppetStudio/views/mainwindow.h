#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>

#include "helpdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void openTreeItem(QModelIndex modelIndex);
    void printToLogs(QString log);

private slots:
    void openWorkspaceTreeContextMenu(const QPoint &point);

    void on_actionNewProject_triggered();

    void on_actionOpenProject_triggered();

    void on_actionSaveAllProjects_triggered();

    void refreshSerialPortsList();

    void on_pushButton_clicked();

    void on_actionHelp_triggered();

private:
    Ui::MainWindow *ui;
    HelpDialog m_helpDialog;
};

#endif // MAINWINDOW_H
