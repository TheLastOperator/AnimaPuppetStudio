#ifndef SEQUENCEFORM_H
#define SEQUENCEFORM_H

#include <QWidget>

#include "mdichild.h"
#include "sequencedriver.h"
#include "models/sequence.h"
#include "models/treeitem.h"

namespace Ui {
class SequenceForm;
}

class SequenceForm : public MdiChild
{
public:
    SequenceForm(Sequence *sequence, QWidget *parent = 0);
    ~SequenceForm();

    TreeItem *treeItem();

public slots:
    void closeView();
    void updateDuration(qint64 duration);
    void updatePosition(qint64 position);

private slots:
    void chooseMusic();
    void addTrack();
    void loadTracks();

private:
    Ui::SequenceForm *ui;
    Sequence *m_sequence;
    SequenceDriver *m_sequenceDriver;
};

#endif // SEQUENCEFORM_H
