#ifndef SEQUENCEDRIVER_H
#define SEQUENCEDRIVER_H

#include <QObject>
#include <QMediaPlayer>

#include "models/sequence.h"

class SequenceDriver : public QObject
{
    Q_OBJECT
public:
    explicit SequenceDriver(Sequence *sequence, QObject *parent = 0);
    ~SequenceDriver();

    qint64 position();
    qint64 duration();

    Sequence *sequence() const;

signals:
    void audioAvailableChanged(bool);
    void durationChanged(qint64);
    void error(QMediaPlayer::Error);
    void mutedChanged(bool);
    void positionChanged(qint64);
    void trackUpdated(int, qint8);
    void stateChanged(QMediaPlayer::State);
    void volumeChanged(int);
    void finished();

public slots:
    void play();
    void pause();
    void stop();
    void togglePlay();
    void pauseAll();
    void setMuted(bool muted);
    void setPosition(qint64 position);
    void setVolume(int volume);

private slots:
    void loadMedia(QString path);
    void updateTimer(qint64 position);
    void updateDuration(qint64 duration);
    void onTimerTick(qint64 tick);

private:
    qint64 m_lastTickPosition;
    Sequence *m_sequence;
    QMediaPlayer *m_player;
    static QList<SequenceDriver*> m_drivers;
};

#endif // SEQUENCEDRIVER_H
