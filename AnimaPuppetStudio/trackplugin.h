#ifndef TRACKPLUGIN_H
#define TRACKPLUGIN_H

#include <QtPlugin>
#include <QString>
#include <QJsonObject>
#include "models/trackdata.h"
#include "views/trackwidget.h"

class TrackPlugin
{
public:
    virtual ~TrackPlugin() {}
    virtual QString type() const = 0;

    virtual TrackData *newTrackData() const = 0;
    virtual TrackData *loadTrackData(QJsonObject data) const = 0;
    virtual QJsonObject saveTrackData(TrackData *data) const = 0;
    virtual TrackWidget *bindWidget(TrackData *data) const = 0;
};
Q_DECLARE_INTERFACE(TrackPlugin, "AnimaPuppetStudio.TrackPlugin")

#endif // TRACKPLUGIN_H
