#include "filemanager.h"
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "models/project.h"
#include "models/trackdata.h"
#include <QMessageBox>

#include "application.h"

FileManager::FileManager(QObject *parent) : QObject(parent)
{
    m_app = static_cast<Application *>(QApplication::instance());
}

FileManager::~FileManager()
{

}

void FileManager::saveProject(Project *project, FileManager::Error &error)
{
    if(project->path() == "") {
        QDir workspaceDir(m_app->settings()->value("workspacePath").value<QString>());

        workspaceDir.setFilter( QDir::Dirs | QDir::NoSymLinks);

        QStringList workspaceDirList = workspaceDir.entryList(QDir::NoDotAndDotDot | QDir::AllDirs);

        int i;
        for(i = 0; workspaceDirList.contains("project" + QString::number(i));i++);
        project->setPath(m_app->settings()->value("workspacePath").value<QString>() + "/project" + QString::number(i) + "/project.anima");
    }

    QDir projectDir(project->path());

    if (!projectDir.exists()) {
        projectDir.mkpath("..");
    }

    QFile projectFile(project->path());
    projectFile.open(QFile::WriteOnly | QFile::Truncate);

    if(projectFile.isOpen()) {

        // Serialize project
        QJsonObject jsonProject;
        jsonProject.insert("name", project->name());

        // Serialize each sequence
        QJsonArray jsonSequences;

        for(Sequence *sequence : project->sequences()) {
            QJsonObject jsonSequence;
            jsonSequence.insert("name", sequence->name());
            jsonSequence.insert("audioPath", sequence->audioPath());

            // Serialize each track
            QJsonArray jsonTracks;

            for(Track *track : sequence->tracks()) {
                QJsonObject jsonTrack;
                jsonTrack.insert("name", track->name());

                // Serialize each receiver
                QJsonArray jsonReceivers;

                for(Receiver *receiver : track->receivers()) {
                    QJsonObject jsonReceiver;
                    jsonReceiver.insert("name", receiver->name());
                    jsonReceiver.insert("uuid", QString("%1").arg(receiver->uuid(), 16, 16, QChar('0')));
                    jsonReceiver.insert("port", receiver->port());
                    jsonReceiver.insert("inverted", receiver->inverted());
                    jsonReceivers.append(jsonReceiver);
                }
                jsonTrack.insert("receivers", jsonReceivers);

                // Serialize each track data
                if(track->trackData() != nullptr) {
                    jsonTrack.insert("type", track->trackData()->type());

                    TrackPlugin *trackPlugin = Application::instance()->trackFactory()->pluginInstance(track->trackData()->type());
                    if(trackPlugin != nullptr) {
                        jsonTrack.insert("data",trackPlugin->saveTrackData(track->trackData()));
                    }
                }

                jsonTracks.append(jsonTrack);
            }

            jsonSequence.insert("tracks", jsonTracks);

            jsonSequences.append(jsonSequence);
        }

        jsonProject.insert("sequences", jsonSequences);

        //Flush

        QJsonDocument jsonDocument;
        jsonDocument.setObject(jsonProject);

        projectFile.write(jsonDocument.toBinaryData());
        projectFile.close();

        error = FileManager::NO_ERROR;
    }

}

void FileManager::saveProject(Project *project)
{
    FileManager::Error e;
    saveProject(project, e);
}

Project *FileManager::loadProject(QString projectPath, Error &error)
{
    if(projectPath == "") {
        error = FileManager::FILE_NAME_EMPTY;
        return nullptr;
    }

    QFile file(projectPath);
    file.open(QIODevice::ReadOnly);

    if(file.isOpen()) {

        QByteArray rawJson = file.readAll();
        file.close();

        QJsonDocument jsonDocument = QJsonDocument::fromBinaryData(rawJson);

        if(jsonDocument.isNull()) {
            error = FileManager::INVALID_FILE_FORMAT;
            return nullptr;
        }

        QJsonObject jsonProject = jsonDocument.object();

        Project *project = new Project(m_app->workspace());

        project->setName(jsonProject.value("name").toString("Unnamed Project"));
        project->setPath(projectPath);

        QJsonArray jsonSequences = jsonProject.value("sequences").toArray();

        for(QJsonValue jsonValueSequence : jsonSequences) {
            QJsonObject jsonSequence = jsonValueSequence.toObject();
            Sequence *sequence = new Sequence(project);
            sequence->setName(jsonSequence.value("name").toString("Unnamed Sequence"));
            sequence->setAudioPath(jsonSequence.value("audioPath").toString());

            QJsonArray jsonTracks = jsonSequence.value("tracks").toArray();

            for(QJsonValue jsonValueTrack : jsonTracks) {
                QJsonObject jsonTrack = jsonValueTrack.toObject();
                Track *track = new Track(sequence);
                track->setName(jsonTrack.value("name").toString("Unnamed Track"));

                QJsonArray jsonReceivers = jsonTrack.value("receivers").toArray();

                for(QJsonValue jsonValueReceiver : jsonReceivers) {
                    QJsonObject jsonReceiver = jsonValueReceiver.toObject();
                    bool ok;
                    Receiver *receiver = new Receiver(track);
                    receiver->setName(jsonReceiver.value("name").toString("Unnamed Receiver"));
                    receiver->setUuid(jsonReceiver.value("uuid").toString("0").toLongLong(&ok, 16));
                    receiver->setPort(jsonReceiver.value("port").toInt(0));
                    receiver->setInverted(jsonReceiver.value("inverted").toBool());
                    track->addReceiver(receiver);
                }

                TrackPlugin *trackPlugin = Application::instance()->trackFactory()->pluginInstance(jsonTrack.value("type").toString());
                if(trackPlugin != nullptr) {
                    track->setTrackData(trackPlugin->loadTrackData(jsonTrack.value("data").toObject()));
                }

                sequence->addTrack(track);
            }

            project->addSequence(sequence);
        }

        error = FileManager::NO_ERROR;

        return project;
    }
    error = FileManager::CANT_OPEN_FILE;
    return nullptr;
}

Project *FileManager::loadProject(QString projectPath)
{
    FileManager::Error e;
    return loadProject(projectPath, e);
}

void FileManager::importOldSequenceFormat(QString filename, Project *project, FileManager::Error &error)
{
    TrackPlugin *trackPlugin = Application::instance()->trackFactory()->pluginInstance("AnalogTrack");
    if(trackPlugin != nullptr) {
        if(filename == "") {//Empty file name
            error = FileManager::FILE_NAME_EMPTY;
            return;
        }

        QFile file(filename);
        file.open(QIODevice::ReadOnly);
        if(file.isOpen()) {
            QDataStream in(&file);
            in.setVersion(QDataStream::Qt_5_4);

            // Read and check the header
            quint32 magic;
            in >> magic;
            if (magic != 0xB0BF17E5) { //bobfiles
                error = FileManager::INVALID_FILE_FORMAT;
                return; //Unknown format
            }

            // Read and check the version
            qint32 version;
            in >> version;

            /* Versions list :
             * 1 = First version, only support labels, chanels and old dataMap format (Deprecated)
             * 2 = Support embeded audio, doesn't support receiver id yet. (Deprecated)
             * 3 = Finaly support receiver id.
             * 4 = Use the new dataMap format (Current)
             */

            if (version < 3) {
                error = FileManager::OLD_FILE_VERSION;
                return; //Format deprecated
            }
            if (version > 4) {
                error = FileManager::NEW_FILE_VERSION;
                return; //Format unsupported
            }

            QByteArray compressedMedia;
            in >> compressedMedia; // Load audio

            Sequence *sequence = new Sequence(project);
            sequence->setName(filename);

            while (!in.atEnd()) { // Load timelines
                QString nameTag;
                QString receiverId;
                qint32 chanelId;
                QMap<qint64, qint8> map;

                in >> nameTag;
                in >> receiverId;
                in >> chanelId;

                if (version < 4) { //For v3 and less
                    QMap<int, int> oldMap;
                    in >> oldMap;

                    QMap<int, int>::iterator i;
                    for (i = oldMap.begin(); i != oldMap.end(); ++i)
                        map.insert(i.key(), i.value());
                }
                else {
                    in >> map;
                }

                //AnalogTrackData *data = new AnalogTrackData();

                //data->setDataMap(map);
                Track *track = new Track(sequence);


                //track->setTrackData(trackPlugin->loadTrackData(jsonTrack.value("data").toObject()));

                track->setName(nameTag);

                Receiver *receiver;
                receiver = new Receiver(track);
                receiver->setName("Recepteur");
                bool ok;
                receiver->setUuid(receiverId.toLongLong(&ok, 16));
                receiver->setPort(chanelId-2);
                receiver->setInverted(false);
                track->addReceiver(receiver);

                sequence->addTrack(track);
            }
            m_app->workspace()->addSequence(project, sequence);
            file.close();
            error = FileManager::NO_ERROR;
            return;
        }
        error = FileManager::CANT_OPEN_FILE;
    }
    else {
        error = FileManager::NO_ERROR;
        QMessageBox messageBox;
        messageBox.setWindowTitle(tr("Echec de l'importation"));
        messageBox.setText(tr("Le plugin \"AnalogTrack\" est requis pour importer des sequences depuis l'ancien format."));
        messageBox.setIcon(QMessageBox::Warning);
        messageBox.exec();
    }
}

void FileManager::openProject(QString projectPath, Error &error)
{
    for(Project* project : m_app->workspace()->projects()) {
        if(project->path() == projectPath) {
            error = FileManager::FILE_ALREADY_OPEN;
            return;
        }
    }
    Project *project = loadProject(projectPath, error);
    if(project != nullptr)
        m_app->workspace()->addProject(project);
}

void FileManager::openProject(QString projectPath)
{
    FileManager::Error e;
    openProject(projectPath, e);
}

void FileManager::newProject(QString projectName, QString directoryPath, FileManager::Error &error)
{
    Project *project = new Project(m_app->workspace());
    project->setName(projectName);
    project->setPath(directoryPath + "/" + projectName + "/" + projectName + ".anima");
    saveProject(project, error);
    if(error == FileManager::NO_ERROR) {
        m_app->workspace()->addProject(project);
    }
    else {
        delete project;
    }
}

void FileManager::newProject(QString projectName, QString directoryPath)
{
    FileManager::Error e;
    newProject(projectName, directoryPath, e);
}

void FileManager::loadWorkspace()
{
    QDir workspaceDir(m_app->settings()->value("workspacePath").value<QString>());

    workspaceDir.setFilter( QDir::Dirs | QDir::NoSymLinks);

    QStringList workspaceDirList = workspaceDir.entryList(QDir::NoDotAndDotDot | QDir::AllDirs);

    for(QString dir : workspaceDirList) {
        openProject(m_app->settings()->value("workspacePath").value<QString>() + "/" + dir + "/project.anima");
    }

}




