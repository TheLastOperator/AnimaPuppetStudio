#ifndef TRACKFACTORY_H
#define TRACKFACTORY_H

#include <QObject>
#include <QMap>
#include "trackplugin.h"

class TrackFactory : public QObject
{
    Q_OBJECT
public:
    explicit TrackFactory(QObject *parent = 0);
    ~TrackFactory();

    TrackPlugin *pluginInstance(QString type);

private:
    QMap<QString, TrackPlugin*> m_plugins;
};

#endif // TRACKFACTORY_H
