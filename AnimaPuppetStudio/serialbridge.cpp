#include "serialbridge.h"

SerialBridge::SerialBridge(QObject *parent) : QObject(parent)
{
    m_serialPort = new QSerialPort(this);
    connect(m_serialPort, SIGNAL(readyRead()), this, SLOT(handleInput()));
}

SerialBridge::~SerialBridge()
{
    m_serialPort->close();
}

void SerialBridge::open(QString name)
{
    m_serialPort->close();
    m_serialPort->setPortName(name);
    if (m_serialPort->open(QIODevice::ReadWrite))
    {
        if(m_serialPort->setBaudRate(QSerialPort::Baud19200) &&
                m_serialPort->setDataBits(QSerialPort::Data8) &&
                m_serialPort->setParity(QSerialPort::NoParity) &&
                m_serialPort->setStopBits(QSerialPort::OneStop) &&
                m_serialPort->setFlowControl(QSerialPort::NoFlowControl))
        {
            if(m_serialPort->isOpen())
            {
                qDebug() << "XBEE: Connected successfully";
                qDebug() << "XBEE: Serial Port Name: " << m_serialPort->portName();
            }
        }
    }
    else
    {
        qDebug() << "XBEE: Serial Port could not be opened";
    }
}

void SerialBridge::sendDataToReceiver(Receiver *receiver, qint8 data)
{
    if(!receiver->updateCache(data)) {
        return;
    }

    QByteArray rawpacket;
    QByteArray packet;

    QDataStream rawpacketStream(&rawpacket, QIODevice::WriteOnly);
    QDataStream packetStream(&packet, QIODevice::WriteOnly);

    // Tx64 Request
    packetStream << (quint8)0x00;

    // Frame ID (no reply requested)
    packetStream << (quint8)0x00;

    // 64DestAddr
    packetStream << (quint64)receiver->uuid();

    // Options
    packetStream << (quint8)0x00;

    // Payload
    packetStream << (quint8)(receiver->port()|128);
    packetStream << (quint8)data;

    /* Build raw packet */

    // Start delimiter
    rawpacketStream << (quint8)0x7E;

    // Length
    rawpacketStream << (quint16)packet.size();

    // Packet
    rawpacketStream.writeRawData(packet.data(), packet.size());

    // Checksum
    rawpacketStream << (quint8)createChecksum(packet);

    if(m_serialPort->isOpen()) {
        qDebug() << "XBEE: TX:" << rawpacket.toHex();
        m_serialPort->write(rawpacket);
        m_serialPort->flush();
    }
    else {
        qDebug() << "XBEE: Serial port closed. Cant send packet";
    }

}

void SerialBridge::handleInput()
{
    m_serialPort->readAll(); // Dirty way of doing this, should check the payload instead
    emit remotePlayRequest();
}

quint8 SerialBridge::createChecksum(QByteArray array){
    int len = array.size();
    unsigned sum = 0x00;
    unsigned ff = 0xFF;
    unsigned fe = 0xFE;
    for(int i=0;i<len;i++)
    {
       unsigned a = array.at(i);
        if (a == 4294967295){
            a = ff;
        } else if (a == 4294967294){
            a = fe;
        }

        sum += a;
    }
    return ((ff - sum) & 0x000000FF);
}
