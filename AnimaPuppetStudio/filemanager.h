#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include "models/project.h"
#include "models/workspace.h"

#include <QObject>

class Application;

class FileManager : public QObject
{
    Q_OBJECT
public:

    enum Error
    {
        NO_ERROR,
        FILE_NAME_EMPTY,
        FILE_ALREADY_OPEN,
        INVALID_FILE_FORMAT,
        OLD_FILE_VERSION,
        NEW_FILE_VERSION,
        CANT_OPEN_FILE
    };

    explicit FileManager(QObject *parent = 0);
    ~FileManager();

    void openProject(QString projectPath, Error &error);
    void openProject(QString projectPath);

    void newProject(QString projectName, QString directoryPath, Error &error);
    void newProject(QString projectName, QString directoryPath);

    void loadWorkspace();

    /***********************************************/

    void saveProject(Project *project, Error &error);
    void saveProject(Project *project);

    Project *loadProject(QString projectPath, Error &error);
    Project *loadProject(QString projectPath);

    // Legacy
    void importOldSequenceFormat(QString filename, Project *project, Error &error);

    Application *m_app;
};

#endif // FILEMANAGER_H
