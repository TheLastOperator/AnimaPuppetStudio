#include "application.h"

#include <QApplication>
#include <QObject>

int main(int argc, char *argv[])
{
    QApplication::setApplicationName("Anima Puppet Studio");
    QApplication::setOrganizationName("Anima Puppet Studio");

    Application a(argc, argv);

    a.window()->showMaximized();

    return a.exec();
}
