#-------------------------------------------------
#
# Project created by QtCreator 2015-12-17T19:40:35
#
#-------------------------------------------------

QT       += core gui network multimedia serialport help

CONFIG += C++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AnimaPuppetStudio
TEMPLATE = app

SOURCES += main.cpp\
    singleapplication.cpp \
    application.cpp \
    filemanager.cpp \
    sequencedriver.cpp \
    trackdriver.cpp \
    analogtrackdriver.cpp \
    serialbridge.cpp \
    trackfactory.cpp \
    models/treeitem.cpp \
    models/workspace.cpp \
    models/project.cpp \
    models/sequence.cpp \
    models/track.cpp \
    models/receiver.cpp \
    views/mdichild.cpp \
    views/projectform.cpp \
    views/sequenceform.cpp \
    views/trackform.cpp \
    views/mainwindow.cpp \
    views/newprojectdialog.cpp \
    views/marqueelabel.cpp \
    views/recieverdialog.cpp \
    views/sortablelistwidget.cpp \
    views/helpdialog.cpp \
    views/helpbrowser.cpp \
    views/outputmonitorwidget.cpp

HEADERS  += singleapplication.h \
    application.h \
    filemanager.h \
    sequencedriver.h \
    trackdriver.h \
    analogtrackdriver.h \
    serialbridge.h \
    trackfactory.h \
    trackplugin.h \
    models/treeitem.h \
    models/workspace.h \
    models/project.h \
    models/sequence.h \
    models/track.h \
    models/trackdata.h \
    models/receiver.h \
    views/mdichild.h \
    views/projectform.h \
    views/sequenceform.h \
    views/trackform.h \
    views/mainwindow.h \
    views/newprojectdialog.h \
    views/marqueelabel.h \
    views/recieverdialog.h \
    views/sortablelistwidget.h \
    views/helpdialog.h \
    views/helpbrowser.h \
    views/trackwidget.h \
    views/outputmonitorwidget.h


FORMS += \
    views/projectform.ui \
    views/sequenceform.ui \
    views/trackform.ui \
    views/mainwindow.ui \
    views/newprojectdialog.ui \
    views/recieverdialog.ui \
    views/helpdialog.ui

RESOURCES += \
    assets.qrc \
    qdarkstyle/style.qrc
