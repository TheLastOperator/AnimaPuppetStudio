#ifndef APPLICACTION_H
#define APPLICACTION_H

#include "singleapplication.h"

#include "filemanager.h"
#include "serialbridge.h"
#include "trackfactory.h"

#include <QSettings>

#include "models/workspace.h"
#include "views/mainwindow.h"

class Application : public SingleApplication
{
public:
    Application(int &argc, char **argv );
    ~Application();

    Workspace *workspace() const;

    QSettings *settings() const;
    void setSettings(QSettings *settings);

    static Application *instance();

    FileManager *fileManager() const;

    MainWindow *window() const;

    SerialBridge *serialBridge() const;

    TrackFactory *trackFactory() const;

private:
    Workspace *m_workspace;
    FileManager *m_fileManager;
    SerialBridge *m_serialBridge;
    TrackFactory *m_trackFactory;
    QSettings *m_settings;
    MainWindow *m_window;
};

#endif // APPLICACTION_H
