#ifndef SERIALBRIDGE_H
#define SERIALBRIDGE_H

#include <QObject>
#include <QtSerialPort>

#include "models/receiver.h"

class SerialBridge : public QObject
{
    Q_OBJECT
public:
    explicit SerialBridge(QObject *parent = 0);
    ~SerialBridge();

signals:
    void remotePlayRequest();

public slots:
    void open(QString name);
    void sendDataToReceiver(Receiver *receiver, qint8 data);

private slots:
    void handleInput();

private:
    QSerialPort *m_serialPort;

    quint8 createChecksum(QByteArray array);
};

#endif // SERIALBRIDGE_H
