#include <avr/sleep.h>
#include <XBee.h>

const int interruptPin = 2;
const int xbeeWakePin = 4;

void wakeUp() {}

XBee xbee = XBee();
uint8_t payload[] = { 0x42, 0x42, 0x42, 0x42 };
XBeeAddress64 addr64 = XBeeAddress64(0x0013A200, 0x40D6B5AF);
Tx64Request tx = Tx64Request(addr64, payload, sizeof(payload));
TxStatusResponse txStatus = TxStatusResponse();
 
void setup()
{
  pinMode(interruptPin, INPUT_PULLUP);
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
  Serial1.begin(9600);
  xbee.setSerial(Serial1);

  pinMode(xbeeWakePin, INPUT);
  
  attachInterrupt(digitalPinToInterrupt(interruptPin), wakeUp, LOW);
  sleep();
}
 
void sleep()
{
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    attachInterrupt(digitalPinToInterrupt(interruptPin),wakeUp, LOW);
    sleep_mode();
    // Continue
    sleep_disable();
    detachInterrupt(digitalPinToInterrupt(interruptPin)); 
}
 
void loop()
{
  digitalWrite(13, HIGH);

  // Wake the xbee
  pinMode(xbeeWakePin, OUTPUT);
  digitalWrite(xbeeWakePin, LOW);

  delay(500);
  
  xbee.send(tx);

  // after sending a tx request, we expect a status response
  // wait up to 5 seconds for the status response
  if (xbee.readPacket(5000)) {
    // got a response!

    // should be a znet tx status              
    if (xbee.getResponse().getApiId() == TX_STATUS_RESPONSE) {
       xbee.getResponse().getTxStatusResponse(txStatus);
      
       // get the delivery status, the fifth byte
         if (txStatus.getStatus() == SUCCESS) {
            // success.  time to celebrate
            digitalWrite(13, LOW);
            delay(200);
            digitalWrite(13, HIGH);
            delay(200);
            digitalWrite(13, LOW);
            delay(200);
            digitalWrite(13, HIGH);
            delay(200);
            digitalWrite(13, LOW);
         } else {
            // the remote XBee did not receive our packet. is it powered on?
            digitalWrite(13, LOW);
            delay(800);
            digitalWrite(13, HIGH);
            delay(800);
            digitalWrite(13, LOW);
            delay(800);
            digitalWrite(13, HIGH);
            delay(800);
            digitalWrite(13, LOW);
         }
      }      
  } else if (xbee.getResponse().isError()) {
    digitalWrite(13, LOW);
    delay(500);
    digitalWrite(13, HIGH);
    delay(1000);
    digitalWrite(13, LOW);
    delay(500);
    digitalWrite(13, HIGH);
    delay(1000);
    digitalWrite(13, LOW);
  } else {
    // local XBee did not provide a timely TX Status Response.  Radio is not configured properly or connected
    digitalWrite(13, LOW);
    delay(1000);
    digitalWrite(13, HIGH);
    delay(1000);
    digitalWrite(13, LOW);
    delay(100);
    digitalWrite(13, HIGH);
    delay(1000);
    digitalWrite(13, LOW);
  }

  pinMode(xbeeWakePin, INPUT);
  
  delay(2000);
  digitalWrite(13, LOW);
  delay(500);
  sleep();
}
