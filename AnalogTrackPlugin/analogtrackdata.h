#ifndef ANALOGTRACKDATA_H
#define ANALOGTRACKDATA_H

#include "../AnimaPuppetStudio/models/trackdata.h"

#include <QMap>

#define TIMELINE_RESOLUTION 100

class AnalogTrackData : public TrackData
{
    Q_OBJECT
public:
    AnalogTrackData();
    ~AnalogTrackData();

    QString type();

    QMap<qint64, qint8> dataMap() const;
    void setDataMap(const QMap<qint64, qint8> &dataMap);

    void addKeyframe(qint64 time, qint8 value);
    void removeKeyframe(qint64 time);
    qint8 valueAt(qint64 time);
    bool isComputedValue(qint64 time);

    qint8 minRecValue() const;
    void setMinRecValue(const qint8 &minRecValue);

    qint8 maxRecValue() const;
    void setMaxRecValue(const qint8 &maxRecValue);

signals:
    void keyframeChanged();

private:
    QMap<qint64,qint8> m_dataMap;
    qint8 m_minRecValue = 0;
    qint8 m_maxRecValue = 127;
};

#endif // ANALOGTRACKDATA_H
