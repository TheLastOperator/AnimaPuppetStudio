#ifndef ANALOGTRACKWIDGET_H
#define ANALOGTRACKWIDGET_H

#include "../AnimaPuppetStudio/views/trackwidget.h"
#include <QMap>
#include <QTimer>

#include "analogtrackdata.h"

class AnalogTrackWidget : public TrackWidget
{
    Q_OBJECT

public:
    explicit AnalogTrackWidget(AnalogTrackData *analogTrackData, QWidget *parent = 0);
    ~AnalogTrackWidget();
    void setViewOffset(qint64 value);
    virtual void setTimeCursor(qint64 value);
    QMenu *configMenu();
signals:
    void valueChanged(qint8 value);
public slots:
    void toggleRecord(bool enable);
protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void mousePressEvent (QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent (QMouseEvent *event) Q_DECL_OVERRIDE;
    bool eventFilter(QObject *obj, QEvent *event);
private slots:
    void updateKeyframes();
    void configMinRecValue();
    void configMaxRecValue();
private:
    void handleMouseEvent (QMouseEvent *event);
    int m_spacing;
    qint64 m_viewOffset;
    qint64 m_progress;
    AnalogTrackData *m_analogTrackData;
    void addKeyFrame(qint64 time, qint8 value);
    void removeKeyFrame(qint64 time);
    bool m_record;
    QTimer *m_timer;
    bool m_isRecKeyDown;
    QMenu *m_configMenu;

};

#endif // ANALOGTRACKWIDGET_H
