#ifndef ANALOGTRACKPLUGIN_H
#define ANALOGTRACKPLUGIN_H

#include "../AnimaPuppetStudio/trackplugin.h"

class AnalogTrackPlugin : public QObject, TrackPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "AnimaPuppetStudio.TrackPlugin" FILE "plugin.json")
    Q_INTERFACES(TrackPlugin)
public:
    virtual QString type() const;
    virtual TrackData *newTrackData() const;
    virtual TrackData *loadTrackData(QJsonObject data) const;
    virtual QJsonObject saveTrackData(TrackData *data) const;
    virtual TrackWidget *bindWidget(TrackData *data) const;
};

#endif // ANALOGTRACKPLUGIN_H
