#include "analogtrackwidget.h"

#include <QTimer>
#include <QPainter>
#include <QMouseEvent>
#include <QMapIterator>
#include <QAction>
#include <QMenu>
#include <QDateTime>
#include <QInputDialog>

#include <QApplication>

#include <QDebug>

AnalogTrackWidget::AnalogTrackWidget(AnalogTrackData *analogTrackData, QWidget *parent) : TrackWidget(parent)
{
    setFixedHeight(148);
    setMouseTracking(true);
    m_analogTrackData = analogTrackData;
    m_spacing = 16;
    m_viewOffset = 0;
    m_progress = 0;
    m_record = false;
    m_isRecKeyDown = false;

    m_timer = new QTimer(this);
    m_timer->setInterval(500);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(repaint()));
    m_timer->start();

    m_configMenu = new QMenu(this);

    QAction* toggleRecordAction = m_configMenu->addAction(tr("Mode enregistrement"));
    toggleRecordAction->setCheckable(true);

    connect(toggleRecordAction, &QAction::toggled, this, &AnalogTrackWidget::toggleRecord);

    QAction* configMinRecValueAction = m_configMenu->addAction("Configurer l'état bas");

    connect(configMinRecValueAction, &QAction::triggered, this, &AnalogTrackWidget::configMinRecValue);

    QAction* configMaxRecValueAction = m_configMenu->addAction("Configurer l'état haut");

    connect(configMaxRecValueAction, &QAction::triggered, this, &AnalogTrackWidget::configMaxRecValue);

    QAction* shiftAction = m_configMenu->addAction("Décaller des clés");
    shiftAction->setDisabled(true);
    QAction* clearAction = m_configMenu->addAction("Supprimer toutes les clés");
    clearAction->setDisabled(true);


    connect(m_analogTrackData, SIGNAL(keyframeChanged()), this, SLOT(updateKeyframes()));

    qApp->installEventFilter(this);

    updateKeyframes();
}

AnalogTrackWidget::~AnalogTrackWidget()
{
}

/**
 * @brief Do the painting
 * @param event QPaint event
 */
void AnalogTrackWidget::paintEvent(QPaintEvent *)
{
    int mouseX = this->mapFromGlobal(QCursor::pos()).x();
    //int mouseY = this->mapFromGlobal(QCursor::pos()).y();

    QPainter painter(this);
    QBrush brushBlack(QColor("#000000"));
    painter.setRenderHint(QPainter::Antialiasing);
    painter.fillRect(rect(), brushBlack);
    QPen penHLines(QColor("#777777"), 1);
    QPen penHLinesWhite(QColor("#FFFFFF"), 2);
    QPen penHLinesRed(QColor("#FF8010"), 2);
    QPen penHLinesCurveGreen(QColor("#89FF1D"), 2);
    QPen penHLinesCurveRed(QColor("#FF891D"), 2);

    for (int i = m_viewOffset; i < m_viewOffset+width()/m_spacing+1; ++i) {
        painter.setPen(i == m_progress ? (m_record ? penHLinesRed : penHLinesWhite) : penHLines);
        painter.drawLine(8 + (i - m_viewOffset) * m_spacing, (i % 10 == 0) ? 0 : 20, 8 + (i-m_viewOffset) * m_spacing, 147);
        if(i%10==0) {
            painter.drawText(16 + (i - m_viewOffset) * m_spacing, 0, 8*m_spacing + (i - m_viewOffset) * m_spacing, 16, 0, QString("%1:%2")
                                                                                                                .arg(i/600, 2, 10, QChar('0'))
                                                                                                                .arg(i/10%60, 2, 10, QChar('0')));
        }
        painter.setPen(i > m_progress ? penHLinesCurveGreen : penHLinesCurveRed);

        qint8 valueAtDrawingTick = m_analogTrackData->valueAt(i * TIMELINE_RESOLUTION);

        if(m_analogTrackData->isComputedValue(i * TIMELINE_RESOLUTION)) {
            painter.drawRect(8 + (i - m_viewOffset) * m_spacing - 1, 147 - valueAtDrawingTick - 1, 2, 2);
        }
        else {
            painter.drawRect(8+(i - m_viewOffset) * m_spacing - 4, 147 - valueAtDrawingTick - 4, 8, 8);
        }

        // Draw lerp
        painter.drawPoint(8 + (i - m_viewOffset) * m_spacing + m_spacing/2, 147 - m_analogTrackData->valueAt((i * TIMELINE_RESOLUTION) + (TIMELINE_RESOLUTION / 2)));

        painter.drawLine(8+(i - m_viewOffset) * m_spacing, 147 - valueAtDrawingTick, 8 + (i - m_viewOffset) * m_spacing, 147);
    }

    if(m_record && (QDateTime::currentDateTime().toMSecsSinceEpoch()/1000)%2==0) {
        painter.setPen(QPen(QColor("#FF0000"), 0));
        painter.setBrush(QColor("#FF0000"));
        painter.drawEllipse(4, 4, 8, 8);
    }

    int mouseTime = m_viewOffset + (mouseX - 8 + m_spacing / 2) / m_spacing;

    qint8 valueAtMouseTime = m_analogTrackData->valueAt(mouseTime * TIMELINE_RESOLUTION);

    painter.fillRect(16 + (mouseTime - m_viewOffset) * m_spacing, 132 - valueAtMouseTime, 24, 16, brushBlack);
    painter.setPen(penHLinesWhite);
    painter.drawText(20 + (mouseTime - m_viewOffset) * m_spacing, 145 - valueAtMouseTime, QString::number(valueAtMouseTime));

}

void AnalogTrackWidget::mousePressEvent(QMouseEvent *event)
{
    handleMouseEvent(event);
}

void AnalogTrackWidget::mouseMoveEvent(QMouseEvent *event)
{
    handleMouseEvent(event);
}

void AnalogTrackWidget::updateKeyframes()
{
    update();
}

void AnalogTrackWidget::configMinRecValue()
{
    bool ok;
    int i = QInputDialog::getInt(this, tr("Configurer les bornes de l'enregistreur"),
                                tr("Valeur de l'état bas:"), m_analogTrackData->minRecValue(), 0, 127, 1, &ok);
    if (ok)
        m_analogTrackData->setMinRecValue(i);
}

void AnalogTrackWidget::configMaxRecValue()
{
    bool ok;
    int i = QInputDialog::getInt(this, tr("Configurer les bornes de l'enregistreur"),
                                tr("Valeur de l'état haut:"), m_analogTrackData->maxRecValue(), 0, 127, 1, &ok);
    if (ok)
        m_analogTrackData->setMaxRecValue(i);
}

/**
 * @brief Handle mouse press and move events
 * @param event QMouseEvent
 */
void AnalogTrackWidget::handleMouseEvent(QMouseEvent *event)
{
    int time = m_viewOffset+(event->pos().x()-8+m_spacing/2)/m_spacing;
    if((time >= m_viewOffset) && (time < m_viewOffset+width()/m_spacing)) {
        if(event->buttons() ==  Qt::LeftButton) {
            addKeyFrame(time, 147 - ((event->pos().y() < 20)?20:event->pos().y()));

        }
        if(event->buttons() ==  Qt::RightButton) {
            removeKeyFrame(time);
        }

    }
}

/**
 * @brief Add a new keyframe at \a time of \a value
 * @param time The key position
 * @param value The key value
 */
void AnalogTrackWidget::addKeyFrame(qint64 time, qint8 value)
{
    m_analogTrackData->addKeyframe(time, value < 0 ? 0 : value);
}

/**
 * @brief Remove a key at \a time
 * @param time Where the key is removed
 */
void AnalogTrackWidget::removeKeyFrame(qint64 time)
{
    m_analogTrackData->removeKeyframe(time);
}

/**
 * @brief Set the offset view at index \a value
 * @param value The offset position
 */
void AnalogTrackWidget::setViewOffset(qint64 value)
{
    m_viewOffset = value;
    update();
}

/**
 * @brief Set the progressbar position
 * @param value Position
 */
void AnalogTrackWidget::setTimeCursor(qint64 time)
{
    m_progress = time / TIMELINE_RESOLUTION;
    int cursorOffset = width() / m_spacing / 3;
    if(m_progress > cursorOffset) {
        m_viewOffset = m_progress - cursorOffset;
    }
    else {
        m_viewOffset = 0;
    }
    if(m_record) {
        m_analogTrackData->addKeyframe(m_progress, m_isRecKeyDown ? m_analogTrackData->maxRecValue() : m_analogTrackData->minRecValue());
    }
    update();
}

QMenu *AnalogTrackWidget::configMenu()
{
    return m_configMenu;
}

void AnalogTrackWidget::toggleRecord(bool enable)
{
    m_record = enable;
}

bool AnalogTrackWidget::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if(keyEvent->key() == Qt::Key_Space)
            m_isRecKeyDown = true;
    }
    if (event->type() == QEvent::KeyRelease)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if(keyEvent->key() == Qt::Key_Space)
            m_isRecKeyDown = false;
    }
    if (event->type() == QEvent::MouseMove)
    {
        update();
    }
    return QObject::eventFilter(obj, event);
}
