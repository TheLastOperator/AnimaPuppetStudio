#include "analogtrackplugin.h"

#include <QStringList>

#include "analogtrackdata.h"
#include "analogtrackwidget.h"

QString AnalogTrackPlugin::type() const
{
    return "AnalogTrack";
}

TrackData *AnalogTrackPlugin::newTrackData() const
{
    return new AnalogTrackData();
}

TrackData *AnalogTrackPlugin::loadTrackData(QJsonObject data) const
{
    AnalogTrackData *trackData = new AnalogTrackData();
    QMap<qint64,qint8> map;
    for(QString jsonKey : data.value("qmap").toObject().keys()) {
        map.insert(jsonKey.toLongLong(), data.value("qmap").toObject().value(jsonKey).toInt());
    }
    trackData->setDataMap(map);
    trackData->setMinRecValue(data.value("minRecValue").toInt(0));
    trackData->setMaxRecValue(data.value("maxRecValue").toInt(127));
    return trackData;
}

QJsonObject AnalogTrackPlugin::saveTrackData(TrackData *data) const
{
    QJsonObject jsonData;

    if(data->type() == "AnalogTrack") {
        QJsonObject jsonQMap;
        for(auto e : static_cast<AnalogTrackData*>(data)->dataMap().toStdMap())
        {
            jsonQMap.insert(QString::number(e.first), e.second);
        }
        jsonData.insert("qmap", jsonQMap);
    }

    jsonData.insert("minRecValue", static_cast<AnalogTrackData*>(data)->minRecValue());
    jsonData.insert("maxRecValue", static_cast<AnalogTrackData*>(data)->maxRecValue());

    return jsonData;
}

TrackWidget *AnalogTrackPlugin::bindWidget(TrackData *data) const
{
    return new AnalogTrackWidget(static_cast<AnalogTrackData*>(data));
}
