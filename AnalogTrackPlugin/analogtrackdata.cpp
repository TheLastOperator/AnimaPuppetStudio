#include "analogtrackdata.h"

AnalogTrackData::AnalogTrackData() : TrackData()
{

}

AnalogTrackData::~AnalogTrackData()
{

}

QString AnalogTrackData::type()
{
    return "AnalogTrack";
}

QMap<qint64, qint8> AnalogTrackData::dataMap() const
{
    return m_dataMap;
}

void AnalogTrackData::setDataMap(const QMap<qint64, qint8> &dataMap)
{
    m_dataMap = dataMap;
}

void AnalogTrackData::addKeyframe(qint64 time, qint8 value)
{
    m_dataMap.insert(time, value < 0 ? 0 : value);
    emit keyframeChanged();
}

void AnalogTrackData::removeKeyframe(qint64 time)
{
    m_dataMap.remove(time);
    emit keyframeChanged();
}

/**
 * @brief Return the maped value for the virtual key at \a index
 * @param index The position of the virtual key
 * @return The computed value
 */
qint8 AnalogTrackData::valueAt(qint64 time)
{
    if(!isComputedValue(time)) {
        return m_dataMap.value(time / TIMELINE_RESOLUTION);
    }

    QMap<qint64, qint8>::iterator keyFramesIterator = m_dataMap.lowerBound(time / TIMELINE_RESOLUTION + (time % TIMELINE_RESOLUTION ? 1 : 0));

    qint64 xa = 0;
    qint64 xb = 0;
    qint8 ya = 0;
    qint8 yb = 0;

    if(keyFramesIterator != m_dataMap.end()) {
        xb = keyFramesIterator.key() * TIMELINE_RESOLUTION;
        yb = keyFramesIterator.value();
        if(keyFramesIterator != m_dataMap.begin()) {
            keyFramesIterator--;
            xa = keyFramesIterator.key() * TIMELINE_RESOLUTION;
            ya = keyFramesIterator.value();
        }
    }
    else {
        if(keyFramesIterator != m_dataMap.begin()) {
            keyFramesIterator--;
            xa = keyFramesIterator.key() * TIMELINE_RESOLUTION;
            ya = keyFramesIterator.value();
            xb = keyFramesIterator.key() * TIMELINE_RESOLUTION;
            yb = keyFramesIterator.value();
        }

    }

    if(xb - xa == 0) {
        return 0;
    }
    else {
       return ya + (time - xa)*(static_cast<double>(yb - ya)/static_cast<double>(xb - xa));
    }

}

bool AnalogTrackData::isComputedValue(qint64 time)
{
    return (time % TIMELINE_RESOLUTION) || !m_dataMap.contains(time / TIMELINE_RESOLUTION);
}
qint8 AnalogTrackData::minRecValue() const
{
    return m_minRecValue;
}

void AnalogTrackData::setMinRecValue(const qint8 &minRecValue)
{
    m_minRecValue = minRecValue;
}
qint8 AnalogTrackData::maxRecValue() const
{
    return m_maxRecValue;
}

void AnalogTrackData::setMaxRecValue(const qint8 &maxRecValue)
{
    m_maxRecValue = maxRecValue;
}


