HEADERS  += \
    analogtrackplugin.h \
    analogtrackdata.h \
    analogtrackwidget.h \
    ../AnimaPuppetStudio/models/trackdata.h \
    ../AnimaPuppetStudio/views/trackwidget.h \
    ../AnimaPuppetStudio/trackplugin.h

TEMPLATE = lib
CONFIG += plugin c++11
TARGET = AnalogTrackPlugin

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += \
    analogtrackplugin.cpp \
    analogtrackdata.cpp \
    analogtrackwidget.cpp
