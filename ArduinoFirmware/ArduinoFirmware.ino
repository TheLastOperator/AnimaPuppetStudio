#include <Servo.h>
#define NBR_SERVOS 12  // the number of servos, up to 48 for Mega, 12 for other boards
#define FIRST_SERVO_PIN 2 

#define NBR_SWITCHES 4
 
Servo Servos[NBR_SERVOS];
static const uint8_t Switches[NBR_SWITCHES] = {A0,A1,A2,A3};

void setup()
{
  for( int i = 0; i < NBR_SERVOS; i++) {
    Servos[i].attach( FIRST_SERVO_PIN +i, 1000, 2000);
  }
  for( int i = 0; i < NBR_SWITCHES; i++) {
    pinMode(Switches[i], OUTPUT);
  }
  Serial.begin(9600);
}
void loop()
{ 
  if(Serial.available()>1) {
    char chanel = Serial.read();
    char value = 0;
    if((chanel & 0x80) == 0x80) {
      chanel = chanel & 0x7f;
      value = Serial.read();
      if(chanel < NBR_SERVOS) {
        Servos[chanel].write(value);
      }
      else if(chanel >= 60 && chanel <= 64) {
        digitalWrite(Switches[chanel-60], value == 0 ? LOW : HIGH);
      }
    }
  }
}
